﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using Transportation_Company.Dtos;
using Transportation_Company.Models;

namespace Transportation_Company.App_Start
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            Mapper.CreateMap<Driver, DriverDto>();
            Mapper.CreateMap<DriverDto, Driver>();

            Mapper.CreateMap<ShippingType, ShippingTypeDto>();
            Mapper.CreateMap<ShippingTypeDto, ShippingType>();

            Mapper.CreateMap<TrailerType, TrailerTypeDto>();
            Mapper.CreateMap<TrailerTypeDto, TrailerType>();

            Mapper.CreateMap<TruckType, TruckTypeDto>();
            Mapper.CreateMap<TruckTypeDto, TruckType>();

            Mapper.CreateMap<Make, MakeDto>();
            Mapper.CreateMap<MakeDto, Make>();

            Mapper.CreateMap<ModelMake, ModelMakeDto>();
            Mapper.CreateMap<ModelMakeDto, ModelMake>();

            Mapper.CreateMap<Customer, CustomerDto>();
            Mapper.CreateMap<CustomerDto, Customer>();
            Mapper.CreateMap<MembershipType, MembershipTypeDto>();
            

        }
    }
}