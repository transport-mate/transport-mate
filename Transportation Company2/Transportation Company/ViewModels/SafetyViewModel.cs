﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Transportation_Company.Models;

namespace Transportation_Company.ViewModels
{
    public class SafetyViewModel
    {
        public IEnumerable<Rental> Rentals { get; set; }

        public IEnumerable<Truck> NeedsMaintenanceTruck { get; set; }

        public IEnumerable<ApplicationUser> Users { get; set; }

        public IEnumerable<ApplicationUser> Drivers { get; set; }

        public IEnumerable<TruckType> TruckTypes { get; set; }

        public IEnumerable<TrailerType> TrailerTypes { get; set; }


    }
}