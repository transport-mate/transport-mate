﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Transportation_Company.Models;

namespace Transportation_Company.ViewModels
{
    public class UpdateTruckMileageViewModel
    {
        public Truck Truck { get; set; }

        public TruckMileageHistory TruckMileageHistory { get; set; }
    }
}