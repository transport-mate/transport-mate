﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Transportation_Company.Models;

namespace Transportation_Company.ViewModels
{
    public class NewPartialModelViewModel
    {
        public IEnumerable<ModelMake> ModelMakes { get; set; }

        public IEnumerable<Make> Makes { get; set; }
    }
}