﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Transportation_Company.Models;

namespace Transportation_Company.ViewModels
{
    public class DriverTruckAndTripsViewModel
    {
        public Truck Truck { get; set; }
        public IEnumerable<Trip> Trips { get; set; }
        public Rental Rental { get; set; }

        public IEnumerable<Trip> FinishedTrips { get; set; }
    }
}