﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Transportation_Company.Models;

namespace Transportation_Company.ViewModels
{
    public class NewPartialTripViewModel
    {
        public IEnumerable<Trip> Trips { get; set; }
        public IEnumerable<ApplicationUser> ApplicationUsers { get; set; }
        public IEnumerable<Order> Orders { get; set; }
        public IEnumerable<Trailer> Trailers { get; set; }
    }
}