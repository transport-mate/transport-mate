﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Transportation_Company.Models;

namespace Transportation_Company.ViewModels
{
    public class TripViewModel
    {
        public Trip Trip { get; set; }

      //  public Order Order { get; set; }

        public IEnumerable<ApplicationUser> ApplicationUsers { get; set; }

        public IEnumerable<Trailer> Trailers { get; set; }

        public IEnumerable<Order> Orders { get; set; }

        public IEnumerable<TripStatus> TripStatuses { get; set; }
    }
}