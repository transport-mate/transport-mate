﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Transportation_Company.Models;

namespace Transportation_Company.ViewModels
{
    public class TrailerViewModel
    {
        public Trailer Trailer { get; set; }

        public IEnumerable<Make> Makes { get; set; }
        public IEnumerable<TrailerType> TrailerTypes { get; set; }

        public IEnumerable<ModelMake> ModelMakes { get; set; }
    }
}