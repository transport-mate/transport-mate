﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Transportation_Company.Models;

namespace Transportation_Company.ViewModels
{
    public class NewModelViewModel
    {
        public IEnumerable<Make> Makes { get; set; }
        public ModelMake ModelMake { get; set; }
    }
}