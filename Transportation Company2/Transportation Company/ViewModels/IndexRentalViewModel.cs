﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Transportation_Company.Controllers;
using Transportation_Company.Models;

namespace Transportation_Company.ViewModels
{
    public class IndexRentalViewModel
    {
        public IEnumerable<Rental> Rentals { get; set; }
        public IEnumerable<ApplicationUser> Drivers { get; set; }
        public IEnumerable<Truck> Trucks { get; set; }  
    }
}