﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Transportation_Company.Models;

namespace Transportation_Company.ViewModels
{
    public class TruckViewModel
    {
        public Truck Truck { get; set; }

        public IEnumerable<TruckType> TruckTypes { get; set; }
        public IEnumerable<Make> Makes { get; set; }
        public IEnumerable<ModelMake> ModelMakes { get; set; }
    }
}