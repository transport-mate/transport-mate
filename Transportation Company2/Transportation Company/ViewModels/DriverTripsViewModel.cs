﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Transportation_Company.Models;

namespace Transportation_Company.ViewModels
{
    public class DriverTripsViewModel
    {
        public IEnumerable<Trip> ActiveTrips { get; set; }
        public IEnumerable<Trip> FinishedTrips { get; set; }
    }
}