﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Transportation_Company.Models;

namespace Transportation_Company.ViewModels
{
    public class EditOrderViewModel
    {
        public Order Order { get; set; }

        public IEnumerable<Customer> Customers { get; set; }
        public IEnumerable<OrderStatus> OrderStatuses { get; set; }
        public IEnumerable<ShippingType> ShippingTypes { get; set; }
    }
}