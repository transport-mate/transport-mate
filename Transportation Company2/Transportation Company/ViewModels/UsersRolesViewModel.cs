﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Transportation_Company.Models;

namespace Transportation_Company.ViewModels
{
    public class UsersRolesViewModel
    {
        public ApplicationUser User { get; set; }
        public string Roles { get; set; }
    }
}