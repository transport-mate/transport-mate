﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Transportation_Company.Models;

namespace Transportation_Company.ViewModels
{
    public class DriverUpdateTripStatusViewModel
    {
        public Trip Trip { get; set; }
        public IEnumerable<TripStatus> TripStatuses { get; set; }
    }
}