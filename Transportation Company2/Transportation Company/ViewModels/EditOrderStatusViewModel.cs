﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Transportation_Company.Models;

namespace Transportation_Company.ViewModels
{
    public class EditOrderStatusViewModel
    {
        public Order Order { get; set; }

        public IEnumerable<OrderStatus> OrderStatuses { get; set; }

    }
}