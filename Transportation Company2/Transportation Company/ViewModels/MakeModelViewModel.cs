﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Transportation_Company.Models;

namespace Transportation_Company.ViewModels
{
    public class MakeModelViewModel
    {
        public IEnumerable<Make> Makes { get; set; }
        public List<ModelMake> ModelMake { get; set; }
    }
}