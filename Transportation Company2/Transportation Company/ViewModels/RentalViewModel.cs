﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Transportation_Company.Controllers;
using Transportation_Company.Models;

namespace Transportation_Company.ViewModels
{
    public class RentalViewModel
    {
        public Rental Rental { get; set; }

        public IEnumerable<Truck> Trucks { get; set; }

        public IEnumerable<ApplicationUser> Drivers { get; set; }   


    }
}