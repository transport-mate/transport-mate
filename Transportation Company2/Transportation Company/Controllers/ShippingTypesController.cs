﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml.Linq;
using Transportation_Company.Models;

namespace Transportation_Company.Controllers
{
    [Authorize(Roles = RoleName.Admin + "," + RoleName.SafetyManager)]
    public class ShippingTypesController : Controller
    {
        private ApplicationDbContext _context;

        public ShippingTypesController()
        {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        // GET: ShippingTypes
        public ViewResult Index()
        {
            var shippingTypes = _context.ShippingTypes;
            
            return View(shippingTypes);
        }

        public ActionResult Details(int id)
        {
            var shippingType = _context.ShippingTypes.SingleOrDefault(s => s.Id == id);

           if (shippingType == null)
                return HttpNotFound();

           return View(shippingType);
        }

        public ActionResult New()
        {
            return View();
        }

        [HttpGet]
        public ActionResult _NewShippingTypePartial()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ShippingType shippingType)
        {
            if (!ModelState.IsValid)
            {
                return View("New");
            }

            _context.ShippingTypes.Add(shippingType);
            _context.SaveChanges();
            TempData["Created"] = "Created";

            return RedirectToAction("Index", "ShippingTypes");
        }

        public ActionResult Edit(int id)
        {
            var shippingType = _context.ShippingTypes.SingleOrDefault(s => s.Id == id);

            if (shippingType == null)
                return HttpNotFound();
                
            return View("EditShipping", shippingType );
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(ShippingType shippingType)
        {
            if (!ModelState.IsValid)
            {
                return View("EditShipping", shippingType);
            }
            var shippingTypeInDb = _context.ShippingTypes.Single(m => m.Id == shippingType.Id);
            shippingTypeInDb.Name = shippingType.Name;
            shippingTypeInDb.Duration = shippingType.Duration;
            shippingTypeInDb.Rate = shippingType.Rate;

            _context.SaveChanges();

            return RedirectToAction("Index", "ShippingTypes");
        }

    }
}