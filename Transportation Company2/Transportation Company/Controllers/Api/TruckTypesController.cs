﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AutoMapper;
using Transportation_Company.Dtos;
using Transportation_Company.Models;

namespace Transportation_Company.Controllers.Api
{
    public class TruckTypesController : ApiController
    {
        private ApplicationDbContext _context;

        public TruckTypesController()
        {
            _context = new ApplicationDbContext();
        }

        // GET /api/trucktypes
        [System.Web.Mvc.Authorize(Roles = RoleName.Admin + "," + RoleName.SafetyManager)]
        public IHttpActionResult GetTruckTypes()
        {
            var truckTypeDtos = _context.TruckTypes
                .ToList()
                .Select(Mapper.Map<TruckType, TruckTypeDto>);

            return Ok(truckTypeDtos);
        }

        // GET api/trucktypes/1
        [System.Web.Mvc.Authorize(Roles = RoleName.Admin + "," + RoleName.SafetyManager)]
        public IHttpActionResult GetTruckType(int id)
        {
            var truckType = _context.TruckTypes.SingleOrDefault(t => t.Id == id);

            if (truckType == null)
                return NotFound();
            return Ok(Mapper.Map<TruckType, TruckTypeDto>(truckType));
        }

        // POST /api/trucktypes
        [HttpPost]
        [System.Web.Mvc.Authorize(Roles = RoleName.Admin + "," + RoleName.SafetyManager)]
        public IHttpActionResult CreateTruckType(TruckTypeDto truckTypeDto)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var truckType = Mapper.Map<TruckTypeDto, TruckType>(truckTypeDto);

            _context.TruckTypes.Add(truckType);
            _context.SaveChanges();

            truckTypeDto.Id = truckType.Id;

            return Created(new Uri(Request.RequestUri + "/" + truckType.Id), truckTypeDto);
        }


        // DELETE /api/trucktypes/1
        [HttpDelete]
        [System.Web.Mvc.Authorize(Roles = RoleName.Admin + "," + RoleName.SafetyManager)]
        public void DeleteTruckType(int id)
        {
            var truckTypeInDb = _context.TruckTypes.SingleOrDefault(t => t.Id == id);

            if (truckTypeInDb == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            _context.TruckTypes.Remove(truckTypeInDb);
            _context.SaveChanges();
        }

        // PUT /api/trucktypes/1
        [HttpPut]
        [System.Web.Mvc.Authorize(Roles = RoleName.Admin + "," + RoleName.SafetyManager)]
        public void UpdateTruckType(int id, TruckTypeDto truckTypeDto)
        {
            if (!ModelState.IsValid)
                throw new HttpResponseException(HttpStatusCode.BadRequest);

            var truckTypeInDb = _context.TruckTypes.SingleOrDefault(t => t.Id == id);

            if (truckTypeInDb == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            Mapper.Map(truckTypeDto, truckTypeInDb);

            _context.SaveChanges();

        }


    }
}
