﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using Transportation_Company.Models;

namespace Transportation_Company.Controllers.Api
{
    public class UsersController : ApiController
    {
        private ApplicationDbContext _context;

        public UsersController()
        {
            _context = new ApplicationDbContext();
        }

        /* // GET /api/users
         public IEnumerable<ApplicationUser> GetUsers(string query = null)
         {
             var driverRoleId = _context.Roles.Where(r => r.Name == "Driver").Select(r => r.Id).SingleOrDefault();
             var drivers = from u in _context.Users where u.Roles.Any(r => r.RoleId == driverRoleId) select u;


             return drivers.ToList();
            // return _context.Users.ToList();
         }*/

        // GET /api/users
        [System.Web.Mvc.Authorize(Roles = RoleName.Admin + "," + RoleName.SafetyManager)]
        public IHttpActionResult GetUsers(string query = null)
        {
            var driverRoleId = _context.Roles.Where(r => r.Name == "Driver").Select(r => r.Id).SingleOrDefault();
            var drivers = from u in _context.Users where u.Roles.Any(r => r.RoleId == driverRoleId) select u;

            var driversQuery = drivers;

           

            if (!String.IsNullOrWhiteSpace(query))
                driversQuery = driversQuery.Where(d => d.FullNameWithBirthDate.Contains(query.ToLower()));

            var driversDtos = driversQuery.ToList();
            

            return Ok(driversDtos);
          
        }

    }
}
