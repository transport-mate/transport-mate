﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AutoMapper;
using Transportation_Company.Dtos;
using Transportation_Company.Models;

namespace Transportation_Company.Controllers.Api
{
    public class ShippingTypesController : ApiController
    {
        private ApplicationDbContext _context;

        public ShippingTypesController()
        {
            _context = new ApplicationDbContext();
        }

        // GET /api/shippingTypes
        [System.Web.Mvc.Authorize(Roles = RoleName.Admin + "," + RoleName.SafetyManager)]
        public IHttpActionResult GetShippingTypes()
        {
            var shippingTypesDtos = _context.ShippingTypes
                .ToList()
                .Select(Mapper.Map<ShippingType, ShippingTypeDto>);

            return Ok(shippingTypesDtos);
        }

        // GET /api/shippingTypes/1
        [System.Web.Mvc.Authorize(Roles = RoleName.Admin + "," + RoleName.SafetyManager)]
        public IHttpActionResult GetShippingType(int id)
        {
            var shippingType = _context.ShippingTypes.SingleOrDefault(s => s.Id == id);

            if (shippingType == null)
                return NotFound();

            return Ok(Mapper.Map<ShippingType, ShippingTypeDto>(shippingType));
        }

        // POST /api/shippingTypes
        [HttpPost]
        [System.Web.Mvc.Authorize(Roles = RoleName.Admin + "," + RoleName.SafetyManager)]
        public IHttpActionResult CreateShippingType(ShippingTypeDto shippingTypeDto)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var shippingType = Mapper.Map<ShippingTypeDto, ShippingType>(shippingTypeDto);

            _context.ShippingTypes.Add(shippingType);
            _context.SaveChanges();

            shippingTypeDto.Id = shippingType.Id;

            return Created(new Uri(Request.RequestUri + "/" + shippingType.Id), shippingTypeDto);
        }

        // PUT /api/shippingTypes/1
        [HttpPut]
        [System.Web.Mvc.Authorize(Roles = RoleName.Admin + "," + RoleName.SafetyManager)]
        public void UpdateShippingType(int id, ShippingTypeDto shippingTypeDto)
        {
            if (!ModelState.IsValid)
                throw new HttpResponseException(HttpStatusCode.BadRequest);

            var shippingTypeInDb = _context.ShippingTypes.SingleOrDefault(s => s.Id == id);

            if (shippingTypeInDb == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            Mapper.Map(shippingTypeDto, shippingTypeInDb);

            _context.SaveChanges();

        }

        // DELETE /api/shippingTypes/1
        [HttpDelete]
        [System.Web.Mvc.Authorize(Roles = RoleName.Admin + "," + RoleName.SafetyManager)]
        public void DeleteShippingType(int id)
        {
            var shippingTypeInDb = _context.ShippingTypes.SingleOrDefault(s => s.Id == id);

            if (shippingTypeInDb == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            _context.ShippingTypes.Remove(shippingTypeInDb);
            _context.SaveChanges();
        }
    }
}
