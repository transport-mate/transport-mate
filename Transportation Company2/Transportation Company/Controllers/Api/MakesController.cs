﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AutoMapper;
using Transportation_Company.Dtos;
using Transportation_Company.Models;

namespace Transportation_Company.Controllers.Api
{
    public class MakesController : ApiController
    {

        private ApplicationDbContext _context;

        public MakesController()
        {
            _context = new ApplicationDbContext();
        }



        // GET /api/makes
        [System.Web.Mvc.Authorize(Roles = RoleName.Admin + "," + RoleName.SafetyManager)]
        public IHttpActionResult GetMakes()
        {
            var makesDtos = _context.Makes
                .ToList()
                .Select(Mapper.Map<Make, MakeDto>);

            return Ok(makesDtos);
        }

        // GET /api/makes/1
        [System.Web.Mvc.Authorize(Roles = RoleName.Admin + "," + RoleName.SafetyManager)]
        public IHttpActionResult GetMake(int id)
        {
            var make = _context.Makes.SingleOrDefault(m => m.Id == id);

            if (make == null)
                return NotFound();

            return Ok(Mapper.Map<Make, MakeDto>(make));
        }

        // POST /api/makes
        [HttpPost]
        [System.Web.Mvc.Authorize(Roles = RoleName.Admin + "," + RoleName.SafetyManager)]
        public IHttpActionResult CreateMake(MakeDto makeDto)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var make = Mapper.Map<MakeDto, Make>(makeDto);

            _context.Makes.Add(make);
            _context.SaveChanges();

            makeDto.Id = make.Id;

            return Created(new Uri(Request.RequestUri + "/" + make.Id), makeDto);
        }

        // PUT /api/makes/1
        [HttpPut]
        [System.Web.Mvc.Authorize(Roles = RoleName.Admin + "," + RoleName.SafetyManager)]
        public void UpdateMake(int id, MakeDto makeDto)
        {
            if (!ModelState.IsValid)
                throw new HttpResponseException(HttpStatusCode.BadRequest);

            var makeInDb = _context.Makes.SingleOrDefault(m => m.Id == id);

            if (makeInDb == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            Mapper.Map(makeDto, makeInDb);

            _context.SaveChanges();
        }


        // DELETE /api/makes/1
        [HttpDelete]
        [System.Web.Mvc.Authorize(Roles = RoleName.Admin + "," + RoleName.SafetyManager)]
        public void DeleteMake(int id)
        {
            var makeInDb = _context.Makes.SingleOrDefault(m => m.Id == id);

            if (makeInDb == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            _context.Makes.Remove(makeInDb);
            _context.SaveChanges();
        }
    }
}
