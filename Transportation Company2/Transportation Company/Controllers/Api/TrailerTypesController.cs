﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AutoMapper;
using Transportation_Company.Dtos;
using Transportation_Company.Models;

namespace Transportation_Company.Controllers.Api
{
    public class TrailerTypesController : ApiController
    {
        private ApplicationDbContext _context;

        public TrailerTypesController()
        {
            _context = new ApplicationDbContext();
        }


        // GET /api/trailertypes
        [System.Web.Mvc.Authorize(Roles = RoleName.Admin + "," + RoleName.SafetyManager)]
        public IHttpActionResult GetTrailerTypes()
        {
            var trailerTypeDtos = _context.TrailerTypes
                .ToList()
                .Select(Mapper.Map<TrailerType, TrailerTypeDto>);

            return Ok(trailerTypeDtos);
        }

        // GET api/trailertypes/1
        [System.Web.Mvc.Authorize(Roles = RoleName.Admin + "," + RoleName.SafetyManager)]
        public IHttpActionResult GetTrailerType(int id)
        {
            var trailerType = _context.TrailerTypes.SingleOrDefault(t => t.Id == id);

            if (trailerType == null)
                return NotFound();
            return Ok(Mapper.Map<TrailerType, TrailerTypeDto>(trailerType));
        }

        // POST /api/trailertypes
        [HttpPost]
        [System.Web.Mvc.Authorize(Roles = RoleName.Admin + "," + RoleName.SafetyManager)]
        public IHttpActionResult CreateTrailerType(TrailerTypeDto trailerTypeDto)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var trailerType = Mapper.Map<TrailerTypeDto, TrailerType>(trailerTypeDto);

            _context.TrailerTypes.Add(trailerType);
            _context.SaveChanges();

            trailerTypeDto.Id = trailerType.Id;

            return Created(new Uri(Request.RequestUri + "/" + trailerType.Id), trailerTypeDto);
        }


        // DELETE /api/trailertypes/1
        [HttpDelete]
        [System.Web.Mvc.Authorize(Roles = RoleName.Admin + "," + RoleName.SafetyManager)]
        public void DeleteTrailerType(int id)
        {
            var trailerTypeInDb = _context.TrailerTypes.SingleOrDefault(t => t.Id == id);

            if (trailerTypeInDb == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            _context.TrailerTypes.Remove(trailerTypeInDb);
            _context.SaveChanges();
        }

        // PUT /api/trailertypes/1
        [HttpPut]
        [System.Web.Mvc.Authorize(Roles = RoleName.Admin + "," + RoleName.SafetyManager)]
        public void UpdateTrailerType(int id, TrailerTypeDto trailerTypeDto)
        {
            if (!ModelState.IsValid)
                throw new HttpResponseException(HttpStatusCode.BadRequest);

            var trailerTypeInDb = _context.TrailerTypes.SingleOrDefault(t => t.Id == id);

            if (trailerTypeInDb == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            Mapper.Map(trailerTypeDto, trailerTypeInDb);

            _context.SaveChanges();

        }


    }
}
