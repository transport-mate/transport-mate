﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Transportation_Company.Models;

namespace Transportation_Company.Controllers.Api
{
    public class TrailersController : ApiController
    {
        private ApplicationDbContext _context;

        public TrailersController()
        {
            _context = new ApplicationDbContext();
        }

        // DELETE /api/trailers/1
        [HttpDelete]
        [System.Web.Mvc.Authorize(Roles = RoleName.Admin + "," + RoleName.SafetyManager)]
        public void DeleteTrailer(int id)
        {
            var trailerInDb = _context.Trailers.SingleOrDefault(t => t.Id == id);

            if (trailerInDb == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            _context.Trailers.Remove(trailerInDb);
            _context.SaveChanges();
        }
    }
}
