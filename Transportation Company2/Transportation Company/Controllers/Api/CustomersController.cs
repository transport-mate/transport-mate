﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AutoMapper;
using Transportation_Company.Dtos;
using Transportation_Company.Models;

namespace Transportation_Company.Controllers.Api
{
    public class CustomersController : ApiController
    {
        private ApplicationDbContext _context;

        public CustomersController()
        {
            _context = new ApplicationDbContext();
        }

        // GET /api/customers
        [System.Web.Mvc.Authorize(Roles = RoleName.Admin + "," + RoleName.Broker)]
        public IHttpActionResult GetCustomers()
        {
            var customerDtos = _context.Customers
                .Include(c => c.MembershipType)
                .ToList()
                .Select(Mapper.Map<Customer, CustomerDto>);

            return Ok(customerDtos);
        }

        // GET api/customers/1
        [System.Web.Mvc.Authorize(Roles = RoleName.Admin + "," + RoleName.Broker)]
        public IHttpActionResult GetCustomer(int id)
        {
            var customer = _context.Customers.SingleOrDefault(c => c.Id == id);

            if (customer == null)
                return NotFound();

            return Ok(Mapper.Map<Customer,CustomerDto>(customer));
        }

        // POST /api/customers
        [HttpPost]
        [System.Web.Mvc.Authorize(Roles = RoleName.Admin + "," + RoleName.Broker)]
        public IHttpActionResult CreateCustomer(CustomerDto customerDto)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var customer = Mapper.Map<CustomerDto, Customer>(customerDto);

            _context.Customers.Add(customer);
            _context.SaveChanges();

            customerDto.Id = customer.Id;

            return Created(new Uri(Request.RequestUri + "/" + customer.Id), customerDto);


        }

        // PUT /api/customers/1
        [HttpPut]
        [System.Web.Mvc.Authorize(Roles = RoleName.Admin + "," + RoleName.Broker)]
        public IHttpActionResult UpdateCustomer(int id, CustomerDto customerDto)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var customerInDb = _context.Customers.SingleOrDefault(c => c.Id == id);

            if (customerInDb == null)
                return NotFound();

            Mapper.Map(customerDto, customerInDb);

           /* customerInDb.Name = customer.Name;
            customerInDb.CompanyName = customer.CompanyName;
            customerInDb.IdentificationNumber = customer.IdentificationNumber;
            customerInDb.Address = customer.Address;
            customerInDb.ZipCode = customer.ZipCode;
            customerInDb.City = customer.City;
            customerInDb.Country = customer.Country;
            customerInDb.Email = customer.Email;
            customerInDb.Comment = customer.Comment;
            customerInDb.PhoneNumber = customer.PhoneNumber;
            customerInDb.PhoneNumberExtension = customer.PhoneNumberExtension;
            customerInDb.BirthDate = customer.BirthDate;
            customerInDb.IsSubscribedToNewsletter = customer.IsSubscribedToNewsletter;
            customerInDb.MembershipTypeId = customer.MembershipTypeId;*/

            _context.SaveChanges();

            return Ok();
        }

        // DELETE /api/customers/1
        [HttpDelete]
        [System.Web.Mvc.Authorize(Roles = RoleName.Admin + "," + RoleName.Broker)]
        public IHttpActionResult DeleteCustomer(int id)
        {
            var customerInDb = _context.Customers.SingleOrDefault(c => c.Id == id);

            if (customerInDb == null)
                return NotFound();

            _context.Customers.Remove(customerInDb);
            _context.SaveChanges();

            return Ok();
        }
    }
}
