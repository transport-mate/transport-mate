﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Transportation_Company.Models;

namespace Transportation_Company.Controllers.Api
{
    public class OrdersController : ApiController
    {
        private ApplicationDbContext _context;

        public OrdersController()
        {
            _context = new ApplicationDbContext();
        }

        // GET /api/orders
        [System.Web.Mvc.Authorize(Roles = RoleName.Admin + "," + RoleName.Broker)]
        public IEnumerable<Order> GetOrders()
        {
            return _context.Orders.ToList();
        }

        // GET api/orders/1
        [System.Web.Mvc.Authorize(Roles = RoleName.Admin + "," + RoleName.Broker)]
        public Order GetOrder(int id)
        {
            var order = _context.Orders.SingleOrDefault(o => o.Id == id);

            if (order == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);
            return order;
        }

        // POST /api/orders
        [HttpPost]
        [System.Web.Mvc.Authorize(Roles = RoleName.Admin + "," + RoleName.Broker)]
        public Order CreateOrder(Order order)
        {
            if (!ModelState.IsValid)
                throw new HttpResponseException(HttpStatusCode.BadRequest);

            _context.Orders.Add(order);
            _context.SaveChanges();

            return order;


        }


        // DELETE /api/orders/1
        [HttpDelete]
        [System.Web.Mvc.Authorize(Roles = RoleName.Admin + "," + RoleName.Broker)]
        public void DeleteOrder(int id)
        {
            var orderInDb = _context.Orders.SingleOrDefault(o => o.Id == id);

            if (orderInDb == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            _context.Orders.Remove(orderInDb);
            _context.SaveChanges();
        }


        // PUT /api/orders/1
        [HttpPut]
        [System.Web.Mvc.Authorize(Roles = RoleName.Admin + "," + RoleName.Broker)]
        public void UpdateOrder(int id, Order order)
        {
            if (!ModelState.IsValid)
                throw new HttpResponseException(HttpStatusCode.BadRequest);

            var orderInDb = _context.Orders.SingleOrDefault(o => o.Id == id);

            if (orderInDb == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            orderInDb.PickUpDate = order.PickUpDate;
            orderInDb.DeliveryDate = order.DeliveryDate;
            orderInDb.PickUpLocation = order.PickUpLocation;
            orderInDb.DeliveryLocation = order.DeliveryLocation;
            orderInDb.Distance = order.Distance;
            orderInDb.IsRefrigerated = order.IsRefrigerated;
            orderInDb.MinTemperature = order.MinTemperature;
            orderInDb.MaxTemperature = order.MaxTemperature;
            orderInDb.ShippingTypeId = order.ShippingTypeId;
            orderInDb.CustomerId = order.CustomerId;
          //  orderInDb.OrderStatusId = order.OrderStatusId;

            _context.SaveChanges();

        }


    }
}
