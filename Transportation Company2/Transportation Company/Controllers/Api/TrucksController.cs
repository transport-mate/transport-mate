﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Transportation_Company.Models;

namespace Transportation_Company.Controllers.Api
{
    public class TrucksController : ApiController
    {
        private ApplicationDbContext _context;

        public TrucksController()
        {
            _context = new ApplicationDbContext();
        }

        // DELETE /api/trucks/1
        [HttpDelete]
        [System.Web.Mvc.Authorize(Roles = RoleName.Admin + "," + RoleName.SafetyManager)]
        public void DeleteTruck(int id)
        {
            var truckInDb = _context.Trucks.SingleOrDefault(t => t.Id == id);

            if (truckInDb == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            _context.Trucks.Remove(truckInDb);
            _context.SaveChanges();
        }
    }
}
