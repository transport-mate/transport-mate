﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Transportation_Company.Models;
using Transportation_Company.Dtos;

namespace Transportation_Company.Controllers.Api
{
    public class RentalsController : ApiController
    {
        private ApplicationDbContext _context;

        public RentalsController()
        {
            _context = new ApplicationDbContext();
        }

        // GET /api/drivers
        [System.Web.Mvc.Authorize(Roles = RoleName.Admin + "," + RoleName.Broker + "," + RoleName.SafetyManager)]
        public IEnumerable<ApplicationUser> GetDrivers()
        {
            return _context.Users.ToList();

        }

        // GET /api/users/1
        [System.Web.Mvc.Authorize(Roles = RoleName.Admin + "," + RoleName.Broker + "," + RoleName.SafetyManager)]
        public ApplicationUser GetDriver(string id)
        {
            var driver = _context.Users.SingleOrDefault(d => d.Id == id);

            if (driver == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }

            return driver;
        }

       [HttpPost]
       [System.Web.Mvc.Authorize(Roles = RoleName.Admin + "," + RoleName.Broker + "," + RoleName.SafetyManager)]
        public IHttpActionResult CreateNewRentals(NewRentalDto newRental)
        {

            if (newRental.TruckIds.Count == 0)
                return BadRequest("No trucks have been given.");


            var driver = _context.Users.SingleOrDefault(d => d.Id == newRental.ApplicationUserId);

            if (driver == null)
                return BadRequest("Driver is not valid");

            
            var trucks = _context.Trucks.Where(t => newRental.TruckIds.Contains(t.Id)).ToList();

            if (trucks.Count != newRental.TruckIds.Count)
                return BadRequest("One or more trucks are invalid or not loaded.");


            foreach (var truck in trucks)
            {
                if (truck.NumberAvailable == 0)
                    return BadRequest("Truck is not available");

                if (truck.IsAvailable == false)
                    return BadRequest("Truck is not available.");

                if (truck.NeedsMaintenance == true)
                    return BadRequest("Truck is being maintained");

                truck.NumberAvailable--;
                
                var rental = new Rental
                {
                  //  ApplicationUser = driver,
                   // Truck = truck,
                    DateRented = DateTime.Now
                };

                
                _context.Rentals.Add(rental);
                truck.IsAvailable = false;

            }

            _context.SaveChanges();

            return Ok();
        }


    }
}
