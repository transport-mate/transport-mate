﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AutoMapper;
using Transportation_Company.Dtos;
using Transportation_Company.Models;

namespace Transportation_Company.Controllers.Api
{
    public class ModelMakesController : ApiController
    {
        private ApplicationDbContext _context;

        public ModelMakesController()
        {
            _context = new ApplicationDbContext();
        }

        // GET /api/modelMakes
        [System.Web.Mvc.Authorize(Roles = RoleName.Admin + "," + RoleName.SafetyManager)]
        public IHttpActionResult GetModels()
        {
            var modelDtos = _context.ModelMakes
                .Include(m => m.Make)
                .ToList()
                .Select(Mapper.Map<ModelMake, ModelMakeDto>);

            return Ok(modelDtos);
        }

        // GET /api/modelMakes/1
        [System.Web.Mvc.Authorize(Roles = RoleName.Admin + "," + RoleName.SafetyManager)]
        public IHttpActionResult GetModelMake(int id)
        {
            var modelMake = _context.ModelMakes.SingleOrDefault(m => m.Id == id);

            if (modelMake == null)
                return NotFound();

            return Ok(Mapper.Map<ModelMake, ModelMakeDto>(modelMake));
        }

        // POST /api/modelMakes
        [HttpPost]
        [System.Web.Mvc.Authorize(Roles = RoleName.Admin + "," + RoleName.SafetyManager)]
        public IHttpActionResult CreateModelMake(ModelMakeDto modelMakeDto)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var modelMake = Mapper.Map<ModelMakeDto, ModelMake>(modelMakeDto);

            _context.ModelMakes.Add(modelMake);
            _context.SaveChanges();

            modelMakeDto.Id = modelMake.Id;

            return Created(new Uri(Request.RequestUri + "/" + modelMake.Id), modelMakeDto);
        }

        // PUT /api/modelMakes/1
        [HttpPut]
        [System.Web.Mvc.Authorize(Roles = RoleName.Admin + "," + RoleName.SafetyManager)]
        public void UpdateModelMake(int id, ModelMakeDto modelMakeDto)
        {
            if (!ModelState.IsValid)
                throw new HttpResponseException(HttpStatusCode.BadRequest);

            var modelMakeInDb = _context.ModelMakes.SingleOrDefault(m => m.Id == id);

            if (modelMakeInDb == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            Mapper.Map(modelMakeDto, modelMakeInDb);

            _context.SaveChanges();
        }

        // DELETE /api/modelMakes/1
        [HttpDelete]
        [System.Web.Mvc.Authorize(Roles = RoleName.Admin + "," + RoleName.SafetyManager)]
        public void DeleteModelMake(int id)
        {
            var modelMakeInDb = _context.ModelMakes.SingleOrDefault(m => m.Id == id);

            if (modelMakeInDb == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            _context.ModelMakes.Remove(modelMakeInDb);
            _context.SaveChanges();
        }
    }
}
