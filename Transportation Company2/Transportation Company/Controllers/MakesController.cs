﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Transportation_Company.Models;
using System.Data.Entity;

namespace Transportation_Company.Controllers
{
    [Authorize(Roles = RoleName.Admin + "," + RoleName.SafetyManager)]
    public class MakesController : Controller
    {
        private ApplicationDbContext _context;

        public MakesController()
        {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        // GET: Makes
        public ViewResult Index()
        {
            var vehicleMake = _context.Makes;

            return View(vehicleMake);
        }

        public ActionResult Details(int id)
        {
            var vehicleMake = _context.Makes.SingleOrDefault(v => v.Id == id);

            if (vehicleMake == null)
                return HttpNotFound();

            return View(vehicleMake);
        }

        public ActionResult NewMake()
        {
            return View();
        }

        public ActionResult _NewMakePartial()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Make make)
        {
            if (!ModelState.IsValid)
            {
                return View("NewMake");
            }

            make.Name = make.Name.ToUpper();

            _context.Makes.Add(make);
            _context.SaveChanges();
            TempData["Success"] = "Success";
            return RedirectToAction("Index", "Makes");
        }


        public ActionResult Edit(int id)
        {
            var make = _context.Makes.SingleOrDefault(m => m.Id == id);

            if (make == null)
                return HttpNotFound();

            return View("EditMake", make);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(Make make)
        {
            if (!ModelState.IsValid)
            {
                return View("EditMake", make);
            }

            var makeInDb = _context.Makes.Single(m => m.Id == make.Id);
            makeInDb.Name = make.Name.ToUpper();

            _context.SaveChanges();

            return RedirectToAction("Index", "Makes");
        }
    }
}