﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Net;
using System.Security.Authentication;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Transportation_Company.Models;
using Transportation_Company.ViewModels;

namespace Transportation_Company.Controllers
{
    public class TripsController : Controller
    {

        private ApplicationDbContext _context;
       
        

        public TripsController()
        {
            _context = new ApplicationDbContext();
            
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        // GET: Trips
        [Authorize(Roles = RoleName.Admin + "," + RoleName.Broker)]
        public ActionResult Index()
        {
            //Selectring users that are only in drivers role. 
           /* var role = _context.Roles.Where(r => r.Name == "Driver").Select(r => r.Id).SingleOrDefault();
           // var drivers = from u in _context.Users where u.Roles.Any(r => r.RoleId == "e4bab7c2-8304-45c8-a953-12b74d2f631c") select u;
            var drivers = from u in _context.Users where u.Roles.Any(r => r.RoleId == role) select u;*/

            var trips = _context.Trips.Where(t => t.TripFinished == null)
                .Include(a => a.Trailer)
                .Include(o => o.Order)
                .Include(d => d.ApplicationUser)
                .ToList();

            var availableTrailers = _context.Trailers
                .Where(t => t.IsAvailable == true && t.NeedsMaintenance == false)
                .ToList();
            var availableOrders = _context.Orders
                .Where(o => o.OrderStatusId == OrderStatus.Accepted)
                .ToList();

            var driverRoleId = _context.Roles.Where(r => r.Name == "Driver").Select(r => r.Id).SingleOrDefault();
            var drivers = from u in _context.Users where u.Roles.Any(r => r.RoleId == driverRoleId) select u;

            var viewModel = new NewPartialTripViewModel
            {
                Trips = trips,
                Trailers = availableTrailers,
                ApplicationUsers = drivers,
                Orders = availableOrders

            };

            return View(viewModel);
        }

        [Authorize(Roles = RoleName.Admin + "," + RoleName.Broker)]
        public ActionResult NewTrip()
        {
           
            var availableTrailers = _context.Trailers
                .Where(t => t.IsAvailable == true && t.NeedsMaintenance == false)
                .ToList();
            var availableOrders = _context.Orders
                .Where(o => o.OrderStatusId == OrderStatus.Accepted)
                .ToList();
            //  var drivers = from u in _context.Users where u.Roles.Any(r => r.RoleId == "e4bab7c2-8304-45c8-a953-12b74d2f631c") select u;
            //  var drivers = _userManager.GetUsersInRoleAsync("Driver").Result; THIS NOT WORKING 
            // var roleId = _context.Roles.Where(r => r.Name == "Driver").Select(a => a.Id); THIS EITHER PROBABLY
            var driverRoleId = _context.Roles.Where(r => r.Name == "Driver").Select(r => r.Id).SingleOrDefault();
            var drivers = from u in _context.Users where u.Roles.Any(r => r.RoleId == driverRoleId) select u;
            

            var viewModel = new TripViewModel
            {
                ApplicationUsers = drivers,
                Trailers = availableTrailers,
                Orders = availableOrders

            };

            return View(viewModel);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = RoleName.Admin + "," + RoleName.Broker)]
        public ActionResult Create(Trip trip)
        {
            if (!ModelState.IsValid)
            {
                var availableTrailers = _context.Trailers
                    .Where(t => t.IsAvailable == true && t.NeedsMaintenance == false)
                    .ToList();
                var availableOrders = _context.Orders
                    .Where(o => o.OrderStatusId == OrderStatus.Accepted)
                    .ToList();
                var driverRoleId = _context.Roles.Where(r => r.Name == "Driver").Select(r => r.Id).SingleOrDefault();
                var drivers = from u in _context.Users where u.Roles.Any(r => r.RoleId == driverRoleId) select u;

                var viewModel = new TripViewModel
                {
                    Trip = trip,
                    ApplicationUsers = drivers,
                    Trailers = availableTrailers,
                    Orders = availableOrders                  
                };
                return View("NewTrip", viewModel);
            }

           
            _context.Trips.Add(trip);

            //Changing seletected Order's status to Assigned from Accepted
            // _context.Orders.First(o => o.Id == trip.OrderId).OrderStatusId = 3; -- inny sposob
            var selectedOrder = _context.Orders.Single(o => o.Id == trip.OrderId);
            selectedOrder.OrderStatusId = OrderStatus.Assigned;

            //Changing trailer status to Not Available after choosing it for particular trip.
            _context.Trailers.First(t => t.Id == trip.TrailerId).IsAvailable = false;

            _context.SaveChanges();
            TempData["Success"] = "Success";


            return RedirectToAction("Index", "Trips");
        }

        [HttpGet]
        [Authorize(Roles = RoleName.Admin + "," + RoleName.Broker + "," + RoleName.Driver)]
        public ActionResult FinishTrip(int id)
        {
            //Take the Trip selected in table
            var trip = _context.Trips.First(t => t.Id == id);
            //Find Order which was taken in that Trip
            var order = _context.Orders.First(o => o.Id == trip.OrderId);

            //If Status of Order in that Trip is not "Delivered" we return 404
            if (order.OrderStatusId != OrderStatus.Delivered)
                return View("InvalidAction");
             //   return HttpNotFound();

            //Otherwise it is Delivered so we can close the Trip.
            //We close the Trip with FinishedDate
            trip.TripFinished = DateTime.Now;
            trip.TripStatusId = TripStatus.Delivered;
            //After the trip is closed we can make Trailer (used for that Trip) AVAILABLE again. 
            _context.Trailers.First(t => t.Id == trip.TrailerId).IsAvailable = true;


            _context.SaveChanges();
            TempData["Finished"] = "Finished";


            if (User.IsInRole(RoleName.Driver))
            {
                return RedirectToAction("Index", "Driver");
            }
           
            
                return RedirectToAction("Index", "Trips");
        }

        [Authorize(Roles = RoleName.Admin + "," + RoleName.Broker)]
        [HttpGet]
        public ActionResult GetFinishedTrips()
        {
            var finishedTrips = _context.Trips.Where(t => t.TripFinished != null)
                .Include(o => o.Order)
                .Include(t => t.Trailer)
                .Include(a => a.ApplicationUser)
                .ToList();

            return View("FinishedTrips", finishedTrips);
        }

        [Authorize(Roles = RoleName.Driver + "," + RoleName.Broker)]
        public ActionResult UpdateTripStatusForm(int id)
        {
            var userId = User.Identity.GetUserId();
            
            var trip = _context.Trips
                .Include(a => a.ApplicationUser)
                .Include(t => t.Trailer)
                .Include(o => o.Order)
                .SingleOrDefault(t => t.Id == id);
                
            if (trip == null)
                return HttpNotFound();

            if (trip.ApplicationUserId != userId)
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
                //throw new AuthenticationException();

            var tripStatuses = _context.TripStatuses
                .Where(t => t.Id == TripStatus.Ready
                            || t.Id == TripStatus.Picked
                            || t.Id == TripStatus.Delivered)
                .ToList();

            var viewModel = new DriverUpdateTripStatusViewModel
            {
                Trip = trip,
                TripStatuses = tripStatuses
            };

            return View("UpdateTripStatus",viewModel);
        }

        [Authorize(Roles = RoleName.Driver + "," + RoleName.Broker)]
        public ActionResult UpdateTripStatus(Trip trip)
        {
            //We select Trip from DB that we selected
            var tripInDb = _context.Trips.Single(t => t.Id == trip.Id);

            //We change the property of trip status 
            tripInDb.TripStatusId = trip.TripStatusId;

            //We look for OrderId in this trip that we changed status in
            var orderFromTripId = _context.Trips.Where(t => t.Id == trip.Id).Select(t => t.OrderId).First();

            //We look in Orders table for Order with Id selected
            var orderFromTrip = _context.Orders.First(o => o.Id == orderFromTripId);

            //Now depending on status of trip updated by driver we update order status
            if (trip.TripStatusId == TripStatus.Picked)
                orderFromTrip.OrderStatusId = OrderStatus.PickedUp;

            if (trip.TripStatusId == TripStatus.Delivered)
                orderFromTrip.OrderStatusId = OrderStatus.Delivered;

            if (trip.TripStatusId == TripStatus.Switched)
                orderFromTrip.OrderStatusId = OrderStatus.Delayed;

            _context.SaveChanges();

           return RedirectToAction("Index", "Driver");
        }
    }
}