﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Transportation_Company.Models;

namespace Transportation_Company.Controllers
{
    [Authorize(Roles = RoleName.Admin + "," + RoleName.SafetyManager)]
    public class AnnouncmentsController : Controller
    {
        private ApplicationDbContext _context;

        public AnnouncmentsController()
        {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }


        // GET: Announcments
        public ActionResult Index()
        {

            var announcments = _context.Announcments
                .Include(a => a.ApplicationUser);

            return View(announcments);
        }

        public ActionResult Details(int id)
        {
            var announcment = _context.Announcments
                .Include(o => o.ApplicationUser)
                .SingleOrDefault(o => o.Id == id);

            if (announcment == null)
                return HttpNotFound();

            return View(announcment);
        }

        public ActionResult NewAnnouncment()
        {
            return View("AnnouncmentForm");
        }

        public ActionResult EditAnnouncment(int id)
        {
            var announcment = _context.Announcments.SingleOrDefault(o => o.Id == id);

            if (announcment == null)
                return HttpNotFound();

            return View("AnnouncmentForm", announcment);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(Announcment announcment)
        {
            /*if (!ModelState.IsValid)
                return View("AnnouncmentForm");*/

            if (announcment.Id == 0)
            {
                if (User.Identity.IsAuthenticated)
                {
                    var userId = User.Identity.GetUserId();
                    announcment.ApplicationUserId = userId;
                }

                _context.Announcments.Add(announcment);
            }

            else
            {
                var announcmentInDb = _context.Announcments.Single(o => o.Id == announcment.Id);

                announcmentInDb.Subject = announcment.Subject;
                announcmentInDb.Message = announcment.Message;
            }

            _context.SaveChanges();

            return RedirectToAction("Index", "Announcments");
        }

      /*  [HttpPost]
        public ActionResult DeleteAnnouncment(int id)
        {
            var announcement = _context.Announcments.SingleOrDefault(o => o.Id == id);

            if (announcement == null)
                return HttpNotFound();

            _context.Announcments.Remove(announcement);
            _context.SaveChanges();

            return RedirectToAction("Index", "Announcments");
        }*/
        
    }
}