﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Transportation_Company.Models;
using System.Data.Entity;
using Transportation_Company.ViewModels;

namespace Transportation_Company.Controllers
{

    public class TrailersController : Controller
    {

        private ApplicationDbContext _context;

        public TrailersController()
        {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }


        // GET: Trailers
        [Authorize(Roles = RoleName.Admin + "," + RoleName.SafetyManager + "," + RoleName.Broker)]
        public ViewResult Index()
        {
            var trailers = _context.Trailers
                .Include(t => t.TrailerType)
                .Include(t => t.Make)
                .Include(t => t.ModelMake)
                .ToList();

            return View(trailers);
        }

        [Authorize(Roles = RoleName.Admin + "," + RoleName.Driver + "," + RoleName.SafetyManager + "," + RoleName.Broker)]
        public ActionResult Details(int id)
        {
            var trailer = _context.Trailers.SingleOrDefault(t => t.Id == id);

            if (trailer == null)
                return HttpNotFound();

            return View(trailer);
        }

        [HttpPost]
        [Authorize(Roles = RoleName.Admin + "," + RoleName.SafetyManager)]
        public ActionResult GetModelsByMake(int makeId)
        {
            var modelsByMake = _context.ModelMakes.Where(m => m.MakeId == makeId).ToList();
            
            return Json(modelsByMake);
        }

        [Authorize(Roles = RoleName.Admin + "," + RoleName.SafetyManager)]
        public ActionResult NewTrailer()
        {
            var makes = _context.Makes.ToList();
            var models = _context.ModelMakes.ToList();
            var trailerTypes = _context.TrailerTypes.ToList();
           

            var viewModel = new TrailerViewModel
            {
                Makes = makes,
                ModelMakes = models,
                TrailerTypes = trailerTypes
                
            };
            return View(viewModel);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = RoleName.Admin + "," + RoleName.SafetyManager)]
        public ActionResult Create(Trailer trailer)
        {
            if (!ModelState.IsValid)
            {
                var viewModel = new TrailerViewModel
                {
                    Trailer = trailer,
                    Makes = _context.Makes.ToList(),
                    TrailerTypes = _context.TrailerTypes.ToList(),
                    ModelMakes = _context.ModelMakes.ToList()
                };
                return View("NewTrailer", viewModel);
            }

            trailer.LicencePlate = trailer.LicencePlate.ToUpper();

            var makeName = _context.Makes.Where(m => m.Id == trailer.MakeId).Select(m => m.Name).First();
            var modelName = _context.ModelMakes.Where(a => a.Id == trailer.ModelMakeId).Select(a => a.Name).First();
            var typeName = _context.TrailerTypes.Where(t => t.Id == trailer.TrailerTypeId).Select(t => t.Name).First();


            _context.Trailers.Add(trailer);
            //We have to add the trailer first, so identity can generate new id, otherwise it will 
            // always be set to 0 in TrailerFullName attribute
            trailer.TrailerFullName = trailer.Id + ", " + makeName + ", " + modelName + ", " + typeName;


            _context.SaveChanges();

            return RedirectToAction("Index", "Trailers");
        }

        [Authorize(Roles = RoleName.Admin + "," + RoleName.SafetyManager)]
        public ActionResult EditTrailer(int id)
        {
            var trailer = _context.Trailers.SingleOrDefault(t => t.Id == id);

            if (trailer == null)
                return HttpNotFound();

            var viewModel = new TrailerViewModel
            {
                Trailer = trailer,
                Makes = _context.Makes.ToList(),
                ModelMakes = _context.ModelMakes.ToList(),
                TrailerTypes = _context.TrailerTypes.ToList()

            };

            return View("EditTrailer", viewModel);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = RoleName.Admin + "," + RoleName.SafetyManager)]
        public ActionResult Save(Trailer trailer)
        {
            if (!ModelState.IsValid)
            {
                var viewModel = new TrailerViewModel
                {
                    Trailer = trailer,
                    Makes = _context.Makes.ToList(),
                    ModelMakes = _context.ModelMakes.ToList(),
                    TrailerTypes = _context.TrailerTypes.ToList()
                };
                return View("EditTrailer", viewModel);
            }

            var trailerInDb = _context.Trailers.Single(t => t.Id == trailer.Id);

            trailerInDb.LicencePlate = trailer.LicencePlate.ToUpper();
            trailerInDb.YearOfProduction = trailer.YearOfProduction;
            trailerInDb.Capacity = trailer.Capacity;
            trailerInDb.Length = trailer.Length;
            trailerInDb.TrailerTypeId = trailer.TrailerTypeId;
            trailerInDb.MakeId = trailer.MakeId;
            trailerInDb.ModelMakeId = trailer.ModelMakeId;

            var makeName = _context.Makes.Where(m => m.Id == trailer.MakeId).Select(m => m.Name).First();
            var modelName = _context.ModelMakes.Where(a => a.Id == trailer.ModelMakeId).Select(a => a.Name).First();
            var typeName = _context.TrailerTypes.Where(t => t.Id == trailer.TrailerTypeId).Select(t => t.Name).First();

            trailerInDb.TrailerFullName = trailer.Id + ", " + makeName + ", " + modelName + ", " + typeName;

            _context.SaveChanges();

            return RedirectToAction("Index", "Trailers");
        }

        [HttpGet]
        [Authorize(Roles = RoleName.Admin + "," + RoleName.SafetyManager)]
        public ActionResult MakeTrailerAvailable(int id)
        {
            var trailerToBeAvailable = _context.Trailers.SingleOrDefault(t => t.Id == id);

            if (trailerToBeAvailable == null)
                return HttpNotFound();

            trailerToBeAvailable.IsAvailable = true;

            _context.SaveChanges();

            return RedirectToAction("Index", "Trailers");
        }

        [HttpGet]
        [Authorize(Roles = RoleName.Admin + "," + RoleName.SafetyManager + "," + RoleName.Broker)]
        public ActionResult MakeTrailerNotAvailable(int id)
        {
            var trailerToBeNotAvailable = _context.Trailers.SingleOrDefault(t => t.Id == id);

            if (trailerToBeNotAvailable == null)
                return HttpNotFound();

            trailerToBeNotAvailable.IsAvailable = false;

            _context.SaveChanges();

            return RedirectToAction("Index", "Trailers");
        }

        [HttpGet]
        [Authorize(Roles = RoleName.Admin + "," + RoleName.Driver + "," + RoleName.SafetyManager + "," + RoleName.Broker)]
        public ActionResult SendTrailerToMechanic(int id)
        {
            var trailerToBeMaintained = _context.Trailers.SingleOrDefault(t => t.Id == id);

            if (trailerToBeMaintained == null)
                return HttpNotFound();

            trailerToBeMaintained.IsAvailable = false;
            trailerToBeMaintained.NeedsMaintenance = true;


            _context.SaveChanges();

            return RedirectToAction("Index", "Trailers");
        }

        [HttpGet]
        [Authorize(Roles = RoleName.Admin + "," + RoleName.SafetyManager)]
        public ActionResult SendTrailerToYard(int id)
        {
            var trailerToBeSentBack = _context.Trailers.SingleOrDefault(t => t.Id == id);

            if (trailerToBeSentBack == null)
                return HttpNotFound();

            trailerToBeSentBack.IsAvailable = true;
            trailerToBeSentBack.NeedsMaintenance = false;


            _context.SaveChanges();

            return RedirectToAction("Index", "Trailers");
        }

        [Authorize(Roles = RoleName.Admin + "," + RoleName.SafetyManager)]
        public ActionResult GetMaintainedTrailers()
        {
            var maintainedTrailers = _context.Trailers.Where(t => t.NeedsMaintenance == true)
                .Include(t => t.TrailerType)
                .Include(t => t.Make)
                .Include(t => t.ModelMake)
                .ToList();

            return View(maintainedTrailers);
        }
    }
}