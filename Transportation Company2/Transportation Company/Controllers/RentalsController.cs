﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Transportation_Company.Models;
using Transportation_Company.ViewModels;

namespace Transportation_Company.Controllers
{
    [Authorize(Roles = RoleName.Admin + "," + RoleName.Broker + "," + RoleName.SafetyManager)]
    public class RentalsController : Controller
    {

        private ApplicationDbContext _context;

        public RentalsController()
        {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }


        [Authorize(Roles = RoleName.Admin + "," + RoleName.Broker + "," + RoleName.SafetyManager)]
        public ViewResult Index()
        {
            var rentals = _context.Rentals
                .Include(r => r.ApplicationUser)
                .Include(r => r.Truck)
                .ToList();

            var trucks = _context.Trucks
                .Where(t => t.IsTaken == false && t.IsAvailable == true && t.NeedsMaintenance == false)
                .ToList();
            var driverRoleId = _context.Roles.Where(r => r.Name == "Driver").Select(r => r.Id).SingleOrDefault();
            var drivers = from u in _context.Users where u.Roles.Any(r => r.RoleId == driverRoleId) select u;

            var viewModel = new IndexRentalViewModel
            {
                Rentals = rentals,
                Trucks = trucks,
                Drivers = drivers
                
            };


            return View(viewModel);
        }

        /*  public ActionResult Details(int id)
          {
              var rental = _context.Rentals.SingleOrDefault(r => r.Id == id);

              if (rental == null)
                  return HttpNotFound();

              return View(rental);
          }*/

        [Authorize(Roles = RoleName.Admin + "," + RoleName.Broker + "," + RoleName.SafetyManager)]
        public ActionResult New()
        {
            var trucks = _context.Trucks
                .Where(t => t.IsTaken == false && t.IsAvailable == true && t.NeedsMaintenance == false)
                .ToList();
            var driverRoleId = _context.Roles.Where(r => r.Name == "Driver").Select(r => r.Id).SingleOrDefault();
            var drivers = from u in _context.Users where u.Roles.Any(r => r.RoleId == driverRoleId) select u;


            var viewModel = new RentalViewModel
            {
                Trucks = trucks,
                Drivers = drivers
            };
            

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = RoleName.Admin + "," + RoleName.Broker + "," + RoleName.SafetyManager)]
        public ActionResult Create(Rental rental)
        {
            if (!ModelState.IsValid)
            {
                var trucks = _context.Trucks
                    .Where(t => t.IsTaken == false && t.IsAvailable == true && t.NeedsMaintenance == false)
                    .ToList();
                var driverRoleId = _context.Roles.Where(r => r.Name == "Driver").Select(r => r.Id).SingleOrDefault();
                var drivers = from u in _context.Users where u.Roles.Any(r => r.RoleId == driverRoleId) select u;
                var viewModel = new RentalViewModel
                {
                    Rental = rental,
                    Trucks = trucks,
                    Drivers = drivers
                };
                return View("New", viewModel);
            }

            _context.Rentals.Add(rental);

            var selectedTruck = _context.Trucks.Single(t => t.Id == rental.TruckId);
            selectedTruck.IsTaken = true;
            selectedTruck.IsAvailable = false;

            _context.SaveChanges();
            TempData["Success"] = "Success";


            return RedirectToAction("Index", "Rentals");
        }

        [Authorize(Roles = RoleName.Admin + "," + RoleName.Broker + "," + RoleName.SafetyManager)]
        public ActionResult FinishRental(int id)
        {
            var rental = _context.Rentals.SingleOrDefault(r => r.Id == id);
            if (rental == null)
                return HttpNotFound();

            var truckInRentalId = _context.Rentals.Where(r => r.Id == id).Select(t => t.TruckId).First();

            var truckInRental = _context.Trucks.First(t => t.Id == truckInRentalId);

            //Actions when rental finished
            rental.DateReturned = DateTime.Now;
            truckInRental.IsTaken = false;
            truckInRental.IsAvailable = true;

            _context.SaveChanges();
            TempData["Finished"] = "Finished";

            return RedirectToAction("Index", "Rentals");
        }


    }
}