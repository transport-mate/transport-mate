﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Transportation_Company.Models;
using Transportation_Company.ViewModels;

namespace Transportation_Company.Controllers
{
    [Authorize(Roles = RoleName.Driver)]
    public class DriverController : Controller
    {

        private ApplicationDbContext _context;

        public DriverController()
        {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        // GET: Driver
        public ActionResult Index()
        {

            var userId = User.Identity.GetUserId();
            var trips = _context.Trips.Count();

            if (trips < 1)
            {
                ViewBag.MyTripsTotal = "No trips were made";
                ViewBag.MyFinishedTripsTotal = "No trips were made";
                ViewBag.MyActiveTripsTotal = "No trips were made";
            }
            else
            {
                var myTripsTotal = _context.Trips.Count(t => t.ApplicationUserId == userId);
                ViewBag.MyTripsTotal = myTripsTotal;
                var myFinishedTripsTotal =
                    _context.Trips.Count(t => t.ApplicationUserId == userId && t.TripFinished != null);
                ViewBag.MyFinishedTripsTotal = myFinishedTripsTotal;

                var myActiveTripsTotal =
                    _context.Trips.Count(t => t.ApplicationUserId == userId && t.TripFinished == null);
                ViewBag.MyActiveTripsTotal = myActiveTripsTotal;
            }


            var trucks = _context.Trucks.Count();
            if (trucks < 1)
            {
                ViewBag.MyTruck = "There are no trucks";
            }


            var rentals = _context.Rentals.Count();
            if (rentals < 1)
            {
                return HttpNotFound();
            }
            
          
            // var myTruckId = _context.Rentals.First(u => u.)

            //Populating announcements on dashboard.
            var lastAnonId = _context.Announcments.Max(a => a.Id);
            var lastAnon = _context.Announcments.FirstOrDefault(a => a.Id == lastAnonId);
            var lastAnonAuthor = _context.Users.SingleOrDefault(a => a.Id == lastAnon.ApplicationUserId);
            if (lastAnon == null)
            {
                ViewBag.AnonSubject = "There are no announcements yet";
                ViewBag.AnonMessage = "Ask your boos to announence something!";
                ViewBag.AnonAuthor = "SysAdmin";
                ViewBag.AnonCreatedOn = DateTime.Now;
            }
            else
            {
                ViewBag.AnonSubject = lastAnon.Subject;
                ViewBag.AnonMessage = lastAnon.Message;
                ViewBag.AnonAuthor = lastAnonAuthor.FirstName + " " + lastAnonAuthor.LastName;
                ViewBag.AnonCreatedOn = lastAnon.CreatedOn;
            }

            var userTrips = _context.Trips
                .Where(a => a.ApplicationUserId == userId)
                .Include(t => t.Trailer)
                .Include(t => t.TripStatus)
                .ToList();

            var rental = _context.Rentals
               .FirstOrDefault(a => a.ApplicationUserId == userId && a.DateReturned == null);

            var myTruck = _context.Rentals
                .FirstOrDefault(a => a.ApplicationUserId == userId && a.DateReturned == null);


            if (myTruck == null || rental == null)
            {
                var rentallNull = _context.Rentals.SingleOrDefault(r => r.Id == 1);
                 var myTruckNull = _context.Trucks.SingleOrDefault(t => t.Id == 1);
                var finishedUserTrips = _context.Trips
                    .Include(t => t.ApplicationUser)
                    .Include(t => t.Order)
                    .Include(t => t.Trailer)
                    .Include(t => t.TripStatus)
                    .Where(t => t.ApplicationUserId == userId && t.TripFinished != null)
                    .ToList();
                var viewModel = new DriverTruckAndTripsViewModel
                {
                    Truck = myTruckNull,
                    Rental = rentallNull,
                    Trips = userTrips,
                    FinishedTrips = finishedUserTrips
                };

                return View(viewModel);
            }
            else
            {
                var myTruckExist = _context.Trucks.Single(t => t.Id == myTruck.Id);
                var finishedUserTrips = _context.Trips
                    .Include(t => t.ApplicationUser)
                    .Include(t => t.Order)
                    .Include(t => t.Trailer)
                    .Include(t => t.TripStatus)
                    .Where(t => t.ApplicationUserId == userId && t.TripFinished != null)
                    .ToList();

                var viewModel = new DriverTruckAndTripsViewModel
                {
                    Truck = myTruckExist,
                    Rental = rental,
                    Trips = userTrips,
                    FinishedTrips = finishedUserTrips
                };

                return View(viewModel);
            }
          

           
          
        }


        public ActionResult MyTrips()
        {
           var userId = User.Identity.GetUserId();
           var activeUsersTrips = _context.Trips
                .Include(t => t.ApplicationUser)
                .Include(t => t.Order)
                .Include(t => t.Trailer)
                .Include(t => t.TripStatus)
                .Where(t => t.ApplicationUserId == userId && t.TripFinished == null)
                .ToList();

            var finishedUserTrips = _context.Trips
                .Include(t => t.ApplicationUser)
                .Include(t => t.Order)
                .Include(t => t.Trailer)
                .Include(t => t.TripStatus)
                .Where(t => t.ApplicationUserId == userId && t.TripFinished != null)
                .ToList();

            var viewModel = new DriverTripsViewModel
            {
                ActiveTrips = activeUsersTrips,
                FinishedTrips = finishedUserTrips
            };

            return View(viewModel);
        }

        public ActionResult MyTruck()
        {

            var userId = User.Identity.GetUserId();

            var myTruck = _context.Rentals
                .Where(a => a.ApplicationUserId == userId && a.DateReturned == null)
                .Include(a => a.Truck)
                .FirstOrDefault();

            return View(myTruck);
        }

        public ActionResult MyOrders()
        {
            var userId = User.Identity.GetUserId();
            var userOrders = _context.Trips
                .Where(t => t.ApplicationUserId == userId)
                .Select(t => t.OrderId)
                .ToList();

           return View(userOrders);
        }

       /* public ActionResult UpdateOrderStatus()
        {
            return View();
        }*/
    }
}