﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Transportation_Company.Models;
using System.Data.Entity;
using System.Threading.Tasks;
using Transportation_Company.ViewModels;

namespace Transportation_Company.Controllers
{
    public class TrucksController : Controller
    {
        private ApplicationDbContext _context;

        public TrucksController()
        {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }
        // GET: Trucks
        [Authorize(Roles = RoleName.Admin + "," + RoleName.SafetyManager + "," + RoleName.Broker)]
        public ActionResult Index()
        {
            var trucks = _context.Trucks
                .Include(t => t.TruckType)
                .Include(t => t.Make)
                .Include(t => t.ModelMake)
                .ToList();
               
            return View(trucks);
        }

        [Authorize(Roles = RoleName.Admin + "," + RoleName.SafetyManager + "," + RoleName.Broker + "," + RoleName.Driver)]
        public ActionResult Details(int id)
        {
            var truck = _context.Trucks.SingleOrDefault(t => t.Id == id);

            if (truck == null)
                return HttpNotFound();

            return View(truck);
        }

        [Authorize(Roles = RoleName.Admin + "," + RoleName.SafetyManager)]
        public ActionResult NewTruck()
        {
            var makes = _context.Makes.ToList();
            var models = _context.ModelMakes.ToList();
            var truckTypes = _context.TruckTypes.ToList();

            var viewModel = new TruckViewModel
            {
                Makes = makes,
                ModelMakes = models,
                TruckTypes = truckTypes

            };
            return View(viewModel);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = RoleName.Admin + "," + RoleName.SafetyManager)]
        public ActionResult Create(Truck truck)
        {
            if (!ModelState.IsValid)
            {
                var viewModel = new TruckViewModel
                {
                    Truck = truck,
                    Makes = _context.Makes.ToList(),
                    TruckTypes = _context.TruckTypes.ToList(),
                    ModelMakes = _context.ModelMakes.ToList()
                };
                return View("NewTruck", viewModel);
            }

            truck.LicencePlate = truck.LicencePlate.ToUpper();
        
            var makeName = _context.Makes.Where(m => m.Id == truck.MakeId).Select(m => m.Name).First();
            var modelName = _context.ModelMakes.Where(model => model.Id == truck.ModelMakeId).Select(m => m.Name).First();
            var typeName = _context.TruckTypes.Where(t => t.Id == truck.TruckTypeId).Select(t => t.Name).First();

            _context.Trucks.Add(truck);
            //We have to add truck to list first in order to have generated Id. Otherwise it will be always 0.
            truck.TruckFullName = truck.Id + ", " + makeName + ", " + modelName + ", " + typeName;


            _context.SaveChanges();

            return RedirectToAction("Index", "Trucks");
        }

        [Authorize(Roles = RoleName.Admin + "," + RoleName.SafetyManager)]
        public ActionResult EditTruck(int id)
        {
            var truck = _context.Trucks.SingleOrDefault(t => t.Id == id);

            if (truck == null)
                return HttpNotFound();

            var viewModel = new TruckViewModel
            {
                Truck = truck,
                Makes = _context.Makes.ToList(),
                ModelMakes = _context.ModelMakes.ToList(),
                TruckTypes = _context.TruckTypes.ToList()

            };

            return View("EditTruck", viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = RoleName.Admin + "," + RoleName.SafetyManager)]
        public ActionResult Save(Truck truck)
        {
            if (!ModelState.IsValid)
            {
                var viewModel = new TruckViewModel
                {
                    Truck = truck,
                    Makes = _context.Makes.ToList(),
                    ModelMakes = _context.ModelMakes.ToList(),
                    TruckTypes = _context.TruckTypes.ToList()
                };

                return View("EditTruck", viewModel);
            }

            var truckInDb = _context.Trucks.Single(t => t.Id == truck.Id);

            truckInDb.LicencePlate = truck.LicencePlate.ToUpper();
            truckInDb.YearOfProduction = truck.YearOfProduction;
            truckInDb.Engine = truck.Engine;
            truckInDb.LiftingCapacity = truck.LiftingCapacity;
            truckInDb.TruckTypeId = truck.TruckTypeId;
            truckInDb.MakeId = truck.MakeId;
            truckInDb.ModelMakeId = truck.ModelMakeId;

            var makeName = _context.Makes.Where(m => m.Id == truck.MakeId).Select(m => m.Name).First();
            var modelName = _context.ModelMakes.Where(model => model.Id == truck.ModelMakeId).Select(m => m.Name).First();
            var typeName = _context.TruckTypes.Where(t => t.Id == truck.TruckTypeId).Select(t => t.Name).First();
       
            truckInDb.TruckFullName = truck.Id + ", " + makeName + ", " + modelName + ", " +
                                      typeName;

            _context.SaveChanges();

            return RedirectToAction("Index", "Trucks");
        }

        [HttpGet]
        [Authorize(Roles = RoleName.Admin + "," + RoleName.SafetyManager)]
        public ActionResult MakeTruckAvailable(int id)
        {
            var truckToBeAvailable = _context.Trucks.SingleOrDefault(t => t.Id == id);

            if (truckToBeAvailable == null)
                return HttpNotFound();

            truckToBeAvailable.IsAvailable = true;
           
            _context.SaveChanges();

            return RedirectToAction("Index", "Trucks");
        }

        [HttpGet]
        [Authorize(Roles = RoleName.Admin + "," + RoleName.SafetyManager + "," + RoleName.Broker)]
        public ActionResult MakeTruckNotAvailable(int id)
        {
            var truckToBeNotAvailable = _context.Trucks.SingleOrDefault(t => t.Id == id);

            if (truckToBeNotAvailable == null)
                return HttpNotFound();

            truckToBeNotAvailable.IsAvailable = false;

            _context.SaveChanges();

            return RedirectToAction("Index", "Trucks");
        }


        [HttpGet]
        [Authorize(Roles = RoleName.Admin + "," + RoleName.SafetyManager + "," + RoleName.Driver + "," + RoleName.Broker)]
        public ActionResult MaintainTruck(int id)
        {
            var truckToBeMaintained = _context.Trucks.SingleOrDefault(t => t.Id == id);

            if (truckToBeMaintained == null)
                return HttpNotFound();

            truckToBeMaintained.IsAvailable = false;
            truckToBeMaintained.NeedsMaintenance = true;


            _context.SaveChanges();

           return RedirectToAction("Index");
        }

        [HttpGet]
        [Authorize(Roles = RoleName.Admin + "," + RoleName.SafetyManager)]
        public ActionResult SendBack(int id)
        {
            var truckToBeUnMaintained = _context.Trucks.SingleOrDefault(t => t.Id == id);

            if (truckToBeUnMaintained == null)
                return HttpNotFound();

            truckToBeUnMaintained.IsAvailable = true;
            truckToBeUnMaintained.NeedsMaintenance = false;


            _context.SaveChanges();

            return RedirectToAction("Index", "Trucks");

        }

        [Authorize(Roles = RoleName.Admin + "," + RoleName.SafetyManager)]
        [HttpGet]
        public ActionResult GetMaintainedTrucks()
        {
            var maintainedTrucks = _context.Trucks.Where(t => t.NeedsMaintenance == true)
                .Include(t => t.TruckType)
                .Include(t => t.Make)
                .Include(t => t.ModelMake)
                .ToList();

            return View(maintainedTrucks);
        }


        public ActionResult UpdateTruckMileage(int id)
        {
            var truck = _context.Trucks.SingleOrDefault(t => t.Id == id);
            if (truck == null)
                return HttpNotFound();

            var viewModel = new UpdateTruckMileageViewModel
            {
                TruckMileageHistory = new TruckMileageHistory()
                {
                    TruckId = truck.Id
                }
               
            };


            return View(viewModel);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult NewTruckMileageEntry(TruckMileageHistory truckHistory)
        {
            if (!ModelState.IsValid)
                return HttpNotFound();

           
            _context.TruckMileageHistory.Add(truckHistory);
            _context.SaveChanges();
           return RedirectToAction("MyTruck", "Driver");
        }

    }
}