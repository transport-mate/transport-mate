﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Metadata.Edm;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Transportation_Company.Models;
using Transportation_Company.ViewModels;

namespace Transportation_Company.Controllers
{
    [Authorize(Roles = RoleName.Admin + "," + RoleName.SafetyManager)]
    public class UsersController : Controller
    {

        private ApplicationDbContext _context;

        public UsersController()
        {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }


       
        //GET: Users
        public ActionResult Index()
        {
             /*if(User.Identity.IsAuthenticated)
            {
                var user = User.Identity;
                ViewBag.Name = user.Name;

                ViewBag.displayMenu = "No";

                if (User.IsInRole("Admin"))
                {
                   // ViewBag.displayMenu = "Admin";
                    return View("UsersList");
                }

                if (User.IsInRole("Broker"))
                {
                   // ViewBag.displayMenu = "Broker";
                    return View();
                }

                if (User.IsInRole("Driver"))
                {
                    ViewBag.displayMenu = "Driver";
                    return View();
                }

                if (User.IsInRole("SafetyManager"))
                {
                    //ViewBag.displayMenu = "SafetyManager";
                    return View();
                }

               
            }
            else
            {
                ViewBag.Name = "Anonymous user! You are not logged in.";
            }*/

           // return View();
            return HttpNotFound();
        }

       
        // GET /UsersList
        public ViewResult UsersList()
        {
            var users = _context.Users.ToList();
            var roles = _context.Roles.ToList();
            var usersWithRoles = users.Select(u => new UsersRolesViewModel
            {
                User = u,
                Roles = String.Join(",", roles.Where(role => role.Users.Any(user => user.UserId == u.Id)).Select(r => r.Name))

            }).ToList();

            return View(usersWithRoles);
        }

        public ActionResult DriversList()
        {
            var driverRoleId = _context.Roles.Where(r => r.Name == "Driver").Select(r => r.Id).SingleOrDefault();
            var drivers = from u in _context.Users where u.Roles.Any(r => r.RoleId == driverRoleId) select u;

            return View(drivers);
        }

       
        public ActionResult Details(string id)
        {
            var user = _context.Users.SingleOrDefault(u => u.Id == id);
         
            if (user == null)
                return HttpNotFound();

            return View(user);
        }

        public FileContentResult GetUserProfileImage(string id)
        {
            var userProfileImage = _context.Users.FirstOrDefault(u => u.Id == id);
            return new FileContentResult(userProfileImage.ProfilePicture, "image/png");
        }

        [Authorize(Roles = RoleName.Admin)]
        public ActionResult UserDelete(string id)
        {
            var user = _context.Users.FirstOrDefault(u => u.Id == id);

            if (user == null)
                return HttpNotFound();

            return View(user);
        }


        [Authorize(Roles = RoleName.Admin)]
        [HttpPost]
        public ActionResult UserDelete(ApplicationUser appuser)
        {
            var user = _context.Users.FirstOrDefault(u => u.Id == appuser.Id);

            if (user == null)
                return HttpNotFound();

            var userInOrder = _context.Orders.Where(u => u.ApplicationUserId == appuser.Id);
            var userInAnon = _context.Announcments.Where(u => u.ApplicationUserId == appuser.Id);
         //   var userInRental = _context.Rentals.Where(u => u.ApplicationUserId == appuser.Id); //then add this below
            if (userInOrder.Any() || userInAnon.Any() ) //add userInRental here.
               return HttpNotFound();


            _context.Users.Remove(user);
            _context.SaveChanges();

            return RedirectToAction("UsersList", "Users");
        }

        
        public ActionResult UserEdit(string id)
        {
            var user = _context.Users.FirstOrDefault(u => u.Id == id);

            if (user == null)
                return HttpNotFound();

            return View(user);
        }

        
        [HttpPost]
        public ActionResult UserEdit(ApplicationUser appuser)
        {
            var user = _context.Users.FirstOrDefault(u => u.Id == appuser.Id);

           // user.ProfilePicture = appuser.ProfilePicture;
            user.FirstName = appuser.FirstName;
            user.LastName = appuser.LastName;
            user.StreetName = appuser.StreetName;
            user.HouseNumber = appuser.HouseNumber;
            user.AptNumber = appuser.AptNumber;
            user.City = appuser.City;
            user.ZipCode = appuser.ZipCode;
            user.BirthDate = appuser.BirthDate;
            user.DrivingLicense = appuser.DrivingLicense;
            user.Email = appuser.Email;
            user.PhoneNumber = appuser.PhoneNumber;
            user.FullNameWithBirthDate = appuser.FirstName + " " + appuser.LastName + ", " +
                                         appuser.BirthDate.ToShortDateString();


            _context.SaveChanges();

            return RedirectToAction("UsersList", "Users");
        }


        public ActionResult BlockUser(string id)
        {
            var user = _context.Users.FirstOrDefault(u => u.Id == id);

            if (user == null)
                return HttpNotFound();

            return View(user);
        }
       

        
        [HttpPost]
        public ActionResult BlockUser(ApplicationUser appuser)
        {
            var user = _context.Users.FirstOrDefault(u => u.Id == appuser.Id);

            if (user == null)
                return HttpNotFound();

            user.IsEnabled = false;
            user.LockoutEnabled = true;
            user.LockoutEndDateUtc = DateTime.MaxValue;

            _context.SaveChanges();

            return RedirectToAction("UsersList", "Users");
        }

        public ActionResult UnblockUser(string id)
        {
            var user = _context.Users.FirstOrDefault(u => u.Id == id);

            if (user == null)
                return HttpNotFound();

            return View(user);
        }

       
        [HttpPost]
        public ActionResult UnblockUser(ApplicationUser appuser)
        {
            var user = _context.Users.FirstOrDefault(u => u.Id == appuser.Id);

            if (user == null)
                return HttpNotFound();

            user.IsEnabled = true;
            user.LockoutEnabled = false;
        

            _context.SaveChanges();

            return RedirectToAction("UsersList", "Users");
        }

       

      /*  public ActionResult ActiveUsers()
        {
            var activeUsers = _context.Users.Where(a => a.LockoutEnabled == false).ToList();
            return View(activeUsers);
        }

        public ActionResult BlockedUsersList()
        {
            var blockedUsers = _context.Users.Where(u => u.LockoutEnabled == true).ToList();
            return View(blockedUsers);
        }*/

       public Boolean isAdminUser()
        {
            if (User.Identity.IsAuthenticated)
            {
                var user = User.Identity;
                ApplicationDbContext context = new ApplicationDbContext();
                var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
                var s = UserManager.GetRoles(user.GetUserId());
                if (s[0].ToString() == "Admin")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return false;
        }


        public Boolean isDriverUser()
        {
            if (User.Identity.IsAuthenticated)
            {
                var user = User.Identity;
                ApplicationDbContext context = new ApplicationDbContext();
                var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
                var s = UserManager.GetRoles(user.GetUserId());
                if (s[2].ToString() == "Driver")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return false;
        }

        public Boolean isBrokerUser()
        {
            if (User.Identity.IsAuthenticated)
            {
                var user = User.Identity;
                ApplicationDbContext context = new ApplicationDbContext();
                var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
                var s = UserManager.GetRoles(user.GetUserId());
                if (s[1].ToString() == "Broker")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return false;
        }

        public Boolean isSafetyUser()
        {
            if (User.Identity.IsAuthenticated)
            {
                var user = User.Identity;
                ApplicationDbContext context = new ApplicationDbContext();
                var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
                var s = UserManager.GetRoles(user.GetUserId());
                if (s[3].ToString() == "SafetyManager")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return false;
        }
    }
}