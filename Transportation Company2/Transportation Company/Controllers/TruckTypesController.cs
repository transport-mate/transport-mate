﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Transportation_Company.Models;

namespace Transportation_Company.Controllers
{
    [Authorize(Roles = RoleName.Admin + "," + RoleName.SafetyManager)]
    public class TruckTypesController : Controller
    {
        private ApplicationDbContext _context;

        public TruckTypesController()
        {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        // GET: TruckTypes
        public ViewResult Index()
        {
            var truckType = _context.TruckTypes;

            return View(truckType);
        }

        public ActionResult Details(int id)
        {
            var truckType = _context.TruckTypes.SingleOrDefault(t => t.Id == id);

            if (truckType == null)
                return HttpNotFound();

            return View(truckType);
        }

        public ActionResult NewTruckType()
        {
            return View();
        }

        public ActionResult _NewTruckTypePartial()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(TruckType truckType)
        {
            if (!ModelState.IsValid)
            {
                return View("NewTruckType");
            }
            _context.TruckTypes.Add(truckType);
            _context.SaveChanges();
            TempData["Created"] = "Created";

            return RedirectToAction("Index", "TruckTypes");
        }

        public ActionResult Edit(int id)
        {
            var truckType = _context.TruckTypes.SingleOrDefault(t => t.Id == id);
            if (truckType == null)
                return HttpNotFound();

            return View("EditTruckType", truckType);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(TruckType truckType)
        {
            if (!ModelState.IsValid)
            {
                return View("EditTruckType", truckType);
            }

            var truckTypeInDb = _context.TruckTypes.Single(t => t.Id == truckType.Id);

            truckTypeInDb.Name = truckType.Name;
            truckTypeInDb.Description = truckType.Description;

            _context.SaveChanges();

            return RedirectToAction("Index", "TruckTypes");
        }
    }
}