﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Identity;
using Rotativa;
using Rotativa.Options;
using Transportation_Company.Models;
using Transportation_Company.ViewModels;


namespace Transportation_Company.Controllers
{
   
    public class OrdersController : Controller
    {


        private ApplicationDbContext _context;

        public OrdersController()
        {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        // GET: Orders
        [Authorize(Roles = RoleName.Admin + "," + RoleName.Broker)]
        public ViewResult Index()
        {
            var orders = _context.Orders
                .Include(o => o.OrderStatus)
                .Include(o => o.ShippingType)
                .Include(o => o.Customer)
                .Include(o => o.ApplicationUser)
                .ToList();
            return View(orders);
        }

        [Authorize(Roles = RoleName.Admin + "," + RoleName.Broker + "," + RoleName.Driver)]
        public ActionResult Details(long id)
        {
            var order = _context.Orders
                .Include(o => o.OrderStatus)
                .Include(o => o.ShippingType)
                .Include(o => o.Customer)
                .Include(o => o.ApplicationUser)
                .Include(o => o.Customer.MembershipType)
                .SingleOrDefault(c => c.Id == id);

            if (order == null)
                return HttpNotFound();

            return View(order);
        }

        [Authorize(Roles = RoleName.Admin + "," + RoleName.Broker)]
        public ActionResult GenerateInvoice(long id)
        {
           
            var orderPDF = _context.Orders
                .Include(o => o.ShippingType)
                .Include(o => o.Customer)
                .Include(o => o.ApplicationUser)
                .Include(o => o.Customer.MembershipType)
                .SingleOrDefault(o => o.Id == id);

          //   var header = Server.MapPath("~/Views/Shared/_Navbar.cshtml");
           // var footer = Server.MapPath("");

           

            /*var switches = string.Format("--disable-smart-shrinking --header-html {0}",
                header);*/
          //  return View("GenerateInvoice", orderPDF);

             return new ViewAsPdf("GenerateInvoice", orderPDF)
             {
                 FileName = "Invoice-" + id + "# - " + DateTime.Now.ToString("yyyyMMdd") + ".pdf",
                 PageSize = Size.A4
               //  PageMargins = new Margins(30,15,20,15),
                // CustomSwitches = switches
             };
        }

        public ActionResult PrintInvoice()
        {
            return new ViewAsPdf("GenerateInvoice");
        }

        [Authorize(Roles = RoleName.Admin + "," + RoleName.Broker)]
        public ActionResult NewOrder()
        {
            var customers = _context.Customers.ToList();
            var shippingTypes = _context.ShippingTypes.ToList();
            // All orderStatus related data in creating new order actions has been removed, because
            // Order status is automatically set to 1 ("Accepted") during creation process.
            // Order status become available to change during Edit process with EditOrderViewModel
          //  var orderStatuses = _context.OrderStatuses.ToList();

            var viewModel = new NewOrderViewModel
            {
                Customers = customers,
                ShippingTypes = shippingTypes
               // OrderStatuses = orderStatuses
            };

            return View(viewModel);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = RoleName.Admin + "," + RoleName.Broker)]
        public ActionResult Create(Order order)
        {

            if (!ModelState.IsValid)
            {
                var viewModel = new NewOrderViewModel
                {
                    Order = order,
                    Customers = _context.Customers.ToList(),
                    ShippingTypes = _context.ShippingTypes.ToList()
                   // OrderStatuses = _context.OrderStatuses.ToList()
                };

                return View("NewOrder", viewModel);
            }

            if (User.Identity.IsAuthenticated)
            {
                var userId = User.Identity.GetUserId();
                order.ApplicationUserId = userId;
            }

            _context.Orders.Add(order);
            _context.SaveChanges();

            return RedirectToAction("Index", "Orders");

        }

        [Authorize(Roles = RoleName.Driver)]
        public ActionResult EditOrderStatus(long id)
        {
            var order = _context.Orders.SingleOrDefault(o => o.Id == id);
            if (order == null)
                return HttpNotFound();

            var orderStatuses = _context.OrderStatuses
                .Where(s => s.Id == OrderStatus.PickedUp
                            || s.Id == OrderStatus.Delayed
                            || s.Id == OrderStatus.Delivered)
                .ToList();

            var viewModel = new EditOrderStatusViewModel
            {
                Order = order,
                OrderStatuses = orderStatuses
            };

            return View(viewModel);
        }

        [Authorize(Roles = RoleName.Driver)]
        public ActionResult SaveStatus(Order order)
        {
            var orderInDb = _context.Orders.Single(o => o.Id == order.Id);
            orderInDb.OrderStatusId = order.OrderStatusId;

            _context.SaveChanges();
            
            return RedirectToAction("Index", "Driver");
        }

        [Authorize(Roles = RoleName.Admin + "," + RoleName.Broker)]
        public ActionResult Edit(long id)
        {
            var order = _context.Orders.SingleOrDefault(o => o.Id == id);

            if (order == null)
                return HttpNotFound();

            var viewModel = new EditOrderViewModel
            {
                Order = order,
                ShippingTypes = _context.ShippingTypes.ToList(),
                Customers = _context.Customers.ToList(),
                OrderStatuses = _context.OrderStatuses.ToList()
            };


            return View("EditOrder", viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = RoleName.Admin + "," + RoleName.Broker)]
        public ActionResult Save(Order order)
        {
            if (!ModelState.IsValid)
            {
                var viewModel = new EditOrderViewModel
                {
                    Order = order,
                    ShippingTypes = _context.ShippingTypes.ToList(),
                    Customers = _context.Customers.ToList(),
                    OrderStatuses = _context.OrderStatuses.ToList()
                };

                return View("EditOrder", viewModel);
            }

            var orderInDb = _context.Orders.Single(o => o.Id == order.Id);

            orderInDb.ShippingTypeId = order.ShippingTypeId;
            orderInDb.CustomerId = order.CustomerId;
            orderInDb.OrderStatusId = order.OrderStatusId;
            orderInDb.DeliveryDate = order.DeliveryDate;
            orderInDb.DeliveryLocation = order.DeliveryLocation;
            orderInDb.DeliveryNumber = order.DeliveryNumber;
            orderInDb.PickUpDate = order.PickUpDate;
            orderInDb.PickUpLocation = order.PickUpLocation;
            orderInDb.PickUpNumber = order.PickUpNumber;
            orderInDb.Distance = order.Distance;
            orderInDb.Height = order.Height;
            orderInDb.Length = order.Length;
            orderInDb.Width = order.Width;
            orderInDb.Weight = order.Weight;
            orderInDb.AdditionalInformation = order.AdditionalInformation;
            orderInDb.IsRefrigerated = order.IsRefrigerated;
            if (order.IsRefrigerated)
            {
                orderInDb.MinTemperature = order.MinTemperature;
                orderInDb.MaxTemperature = order.MaxTemperature;
            }
            else
            {
                orderInDb.MinTemperature = null;
                orderInDb.MaxTemperature = null;
            }
           


            _context.SaveChanges();


            return RedirectToAction("Index", "Orders");
        }

    }
}