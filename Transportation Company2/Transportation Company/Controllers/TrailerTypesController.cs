﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Transportation_Company.Models;

namespace Transportation_Company.Controllers
{
    [Authorize(Roles = RoleName.Admin + "," + RoleName.SafetyManager)]
    public class TrailerTypesController : Controller
    {
        private ApplicationDbContext _context;

        public TrailerTypesController()
        {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        // GET: TrailerTypes
        public ViewResult Index()
        {
            var trailerTypes = _context.TrailerTypes;

            return View(trailerTypes);
        }

        public ActionResult Details(int id)
        {
            var trailerType = _context.TrailerTypes.SingleOrDefault(s => s.Id == id);

            if (trailerType == null)
                return HttpNotFound();

            return View(trailerType);
        }

        public ActionResult NewTrailerType()
        {
            return View();
        }

        public ActionResult _NewTrailerTypePartial()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(TrailerType trailerType)
        {
            if (!ModelState.IsValid)
            {
                return View("NewTrailerType");
            }

            _context.TrailerTypes.Add(trailerType);
            _context.SaveChanges();
            TempData["Created"] = "Created";

            return RedirectToAction("Index", "TrailerTypes");
        }

        public ActionResult Edit(int id)
        {
            var trailerType = _context.TrailerTypes.SingleOrDefault(t => t.Id == id);
            if (trailerType == null)
                return HttpNotFound();

            return View("EditTrailerType", trailerType);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(TrailerType trailerType)
        {
            if (!ModelState.IsValid)
            {
                return View("EditTrailerType", trailerType);
            }

            var trailerTypeInDb = _context.TrailerTypes.Single(t => t.Id == trailerType.Id);

            trailerTypeInDb.Name = trailerType.Name;
            trailerTypeInDb.Description = trailerType.Description;

            _context.SaveChanges();

            return RedirectToAction("Index", "TrailerTypes");
        }
    }
}