﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Transportation_Company.Models;
using Transportation_Company.ViewModels;

namespace Transportation_Company.Controllers
{
    [Authorize(Roles = RoleName.SafetyManager)]
    public class SafetyController : Controller
    {
        private ApplicationDbContext _context;

        public SafetyController()
        {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }


        // GET: Safety
        public ActionResult Index()
        {
            //Vehicles based statistics.
            var trailersCount = _context.Trailers.Count();
            var trucksCount = _context.Trucks.Count();
            var vehiclesCount = trucksCount + trailersCount;
            ViewBag.TrailersCount = trailersCount;
            ViewBag.TrucksCount = trucksCount;
            ViewBag.VehiclesCount = vehiclesCount;

            //Maintenance stats
            var trucksMaintenanceNumber = _context.Trucks.Count(t => t.NeedsMaintenance == false);
            ViewBag.NeedMaintenanceNumber = trucksMaintenanceNumber;

            //Shippings
            var shippingCount = _context.ShippingTypes.Count();
            ViewBag.ShippingNumber = shippingCount;

            //Brands and models count
            var brandsCount = _context.Makes.Count();
            var modelsCount = _context.ModelMakes.Count();
            ViewBag.BrandsCount = brandsCount;
            ViewBag.ModelsCount = modelsCount;

            //Users
            var usersCount = _context.Users.Count();
            var activeUsers = _context.Users.Count(a => a.IsEnabled == true);
            var blockedUsers = _context.Users.Count(a => a.IsEnabled == false);
            ViewBag.UsersCount = usersCount;
            ViewBag.ActiveUsers = activeUsers;
            ViewBag.BlockedUsers = blockedUsers;

            //Top brand of Truck
            var topTruckBrand = _context.Trucks
                .GroupBy(t => t.MakeId)
                .OrderByDescending(tp => tp.Count())
                .Take(1)
                .Select(t => t.Key)
                .First();

            var topTruckBrandName = _context.Makes.Single(b => b.Id == topTruckBrand);

            ViewBag.TopTruckBrandName = topTruckBrandName.Name;

            //Top brand of trailers
            var topTrailerBrand = _context.Trailers
                .GroupBy(t => t.MakeId)
                .OrderByDescending(tp => tp.Count())
                .Take(1)
                .Select(t => t.Key)
                .First();

            var topTrailerBrandName = _context.Makes.Single(b => b.Id == topTrailerBrand);

            ViewBag.TopTrailerBrandName = topTrailerBrandName.Name;

            //Rentals stats
            var activeRentals = _context.Rentals.Count(r => r.DateReturned == null);
            ViewBag.ActiveRentals = activeRentals;
            var rentalsAll = _context.Rentals.Count();
            ViewBag.AllRentals = rentalsAll;



            //Populating announcements on dashboard.
            var lastAnonId = _context.Announcments.Max(a => a.Id);
            var lastAnon = _context.Announcments.FirstOrDefault(a => a.Id == lastAnonId);
            var lastAnonAuthor = _context.Users.SingleOrDefault(a => a.Id == lastAnon.ApplicationUserId);
            if (lastAnon == null)
            {
                ViewBag.AnonSubject = "There are no announcements yet";
                ViewBag.AnonMessage = "Ask your boos to announence something!";
                ViewBag.AnonAuthor = "SysAdmin";
                ViewBag.AnonCreatedOn = DateTime.Now;
            }
            else
            {
                ViewBag.AnonSubject = lastAnon.Subject;
                ViewBag.AnonMessage = lastAnon.Message;
                ViewBag.AnonAuthor = lastAnonAuthor.FirstName + " " + lastAnonAuthor.LastName;
                ViewBag.AnonCreatedOn = lastAnon.CreatedOn;
            }

            var driverRoleId = _context.Roles.Where(r => r.Name == "Driver").Select(r => r.Id).SingleOrDefault();
            var drivers = from u in _context.Users where u.Roles.Any(r => r.RoleId == driverRoleId) select u;

            var trucksToMaintanance = _context.Trucks.Where(t => t.NeedsMaintenance == true).ToList();
            var activeRentalsList = _context.Rentals
                .Where(r => r.DateReturned == null)
                .Include(a => a.ApplicationUser)
                .Include(t => t.Truck)
                .ToList();

            var viewModel = new SafetyViewModel
            {
                Drivers = drivers,
                Users = _context.Users.ToList(),
                TruckTypes = _context.TruckTypes.ToList(),
                TrailerTypes = _context.TrailerTypes.ToList(),
                NeedsMaintenanceTruck = trucksToMaintanance,
                Rentals = activeRentalsList
            };

            return View(viewModel);
        }
    }
}