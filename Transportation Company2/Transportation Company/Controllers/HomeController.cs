﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Transportation_Company.Models;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace Transportation_Company.Controllers
{
    
    public class HomeController : Controller
    {
        private ApplicationDbContext _context;

        public HomeController()
        {
            _context = new ApplicationDbContext();
        }

        [AllowAnonymous]
        public ActionResult Index()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult About()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult Contact()
        {
            return View();
        }

        public ActionResult Blocked()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public async Task<ActionResult> Contact(EmailFormModel model)
        {
            if (ModelState.IsValid)
            {
                var body = "<p>Email via application contact form from: {0} ({1})</p><p>Message:</p><p>{2}</p>";
                var message = new MailMessage();
                message.To.Add(new MailAddress("recipient@gmail.com")); // replace it with valid email address of where you want it to be sent
                message.From = new MailAddress("sender@outlook"); // replace it with email that will send this data from this form.
                message.Subject = "Application Contact form";
                message.Body = string.Format(body, model.FromName, model.FromEmail, model.Message);
                message.IsBodyHtml = true;

                using (var smtp = new SmtpClient())
                {
                    var credential = new NetworkCredential
                    {
                        UserName = "", //email username
                        Password = "" // email passport
                    };

                    smtp.Credentials = credential;
                    smtp.Host = "smtp-mail.outlook.com"; //smtp host address
                    smtp.Port = 587; //smtp email port
                    smtp.EnableSsl = true;
                    await smtp.SendMailAsync(message);
                    return RedirectToAction("Sent");
                }
            }
            return View(model);
        }

        [AllowAnonymous]
        public ActionResult Sent()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Search(string trackingNumber)
        {
            

            if (string.IsNullOrWhiteSpace(trackingNumber))
                return View("WrongTrackingNumber");

            var exist = _context.Orders.Any(o => o.TrackingNumber.ToString() == trackingNumber);

            if (!exist)
                return View("WrongTrackingNumber");

            var order = _context.Orders
                .Where(o => o.TrackingNumber.ToString() == trackingNumber)
                .Include(o => o.OrderStatus);

            return View("Search_result", order);
        }

        public FileContentResult ProfilePictures()
        {
            if (User.Identity.IsAuthenticated)
            {
                string userId = User.Identity.GetUserId();

                if (userId == null)
                {
                    string fileName = HttpContext.Server.MapPath(@"~/Content/Images/Unknown.png");

                    byte[] imageData = null;
                    FileInfo fileInfo = new FileInfo(fileName);
                    long imageFileLength = fileInfo.Length;

                    FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
                    BinaryReader br = new BinaryReader(fs);
                    imageData = br.ReadBytes((int) imageFileLength);
                    return File(imageData, "image/png");

                }

                // to get the user details to load user image 

                var userImage = _context.Users.FirstOrDefault(x => x.Id == userId);

                if (userImage.ProfilePicture == null)
                {
                    string fileName = HttpContext.Server.MapPath(@"~/Content/Images/Unknown.png");

                    byte[] imageData = null;
                    FileInfo fileInfo = new FileInfo(fileName);

                    long imageFileLength = fileInfo.Length;
                    FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
                    BinaryReader br = new BinaryReader(fs);

                    imageData = br.ReadBytes((int)imageFileLength);
                    userImage.ProfilePicture = imageData;

                    // return File(imageData, "image/png");
                }

                return new FileContentResult(userImage.ProfilePicture, "image/png");

            }

            else
            {
                string fileName = HttpContext.Server.MapPath(@"~/Content/Images/Unknown.png");

                byte[] imageData = null;
                FileInfo fileInfo = new FileInfo(fileName);

                long imageFileLength = fileInfo.Length;
                FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
                BinaryReader br = new BinaryReader(fs);

                imageData = br.ReadBytes((int) imageFileLength);
                return File(imageData, "image/png");
            }



        }

        }

    }
