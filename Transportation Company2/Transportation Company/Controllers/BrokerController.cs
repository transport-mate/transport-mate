﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Transportation_Company.Models;

namespace Transportation_Company.Controllers
{
    [Authorize(Roles = RoleName.Broker)]
    public class BrokerController : Controller
    {
        private ApplicationDbContext _context;

        public BrokerController()
        {
            _context = new ApplicationDbContext();
        }
        // GET: Broker
        public ActionResult Index()
        {
            //Most valuable customer
            var ordersInDbNumber = _context.Orders.Count();

            if (ordersInDbNumber == 0)
            {
                ViewBag.MostValuedCustomer = "Mr. Nobody";
                ViewBag.MvpOrdersTotal = 0;
            }
            else
            {
                var mostValuedCustomerId = _context.Orders
                    .GroupBy(a => a.CustomerId)
                    .OrderByDescending(g => g.Count())
                    .Take(1)
                    .Select(k => k.Key)
                    .First();
                var mvp = _context.Customers.SingleOrDefault(a => a.Id == mostValuedCustomerId);
                ViewBag.MostValuedCustomer = mvp.CompanyName;
                var mvpOrdersTotal = _context.Orders.Count(a => a.CustomerId == mvp.Id);
                ViewBag.MvpOrdersTotal = mvpOrdersTotal;
            }


            //The best broker
            if (ordersInDbNumber == 0)
            {
                ViewBag.TheBestBrokerFullName = "Mr. Nobody";
                ViewBag.NumberOfBestBrokersOrders = 0;
            }
            else
            {
                var theBestBroker = _context.Orders
                    .GroupBy(o => o.ApplicationUserId)
                    .OrderByDescending(g => g.Count())
                    .Take(1)
                    .Select(t => t.Key)
                    .First();
                var theBestBrokerFullName = _context.Users.Single(u => u.Id == theBestBroker);
                var numberOfBestBrokersOrders = _context.Orders.Count(o => o.ApplicationUserId == theBestBroker);
                ViewBag.TheBestBrokerFullName = theBestBrokerFullName.FirstName + " " + theBestBrokerFullName.LastName; ;
                ViewBag.NumberOfBestBrokersOrders = numberOfBestBrokersOrders;
            }


            //Orders statistics
            ViewBag.OrdersTotal = GetOrdersTotal();
            var userId = User.Identity.GetUserId();
            ViewBag.BrokersOrdersTotal = GetBrokersOrdersTotal(userId);


            var deliveredOrdersCount = _context.Orders.Count(o => o.OrderStatusId == OrderStatus.Delivered);
            var onwayOrders = _context.Orders.Count(o => o.OrderStatusId == OrderStatus.Assigned);
            ViewBag.DeliveredOrdersNumber = deliveredOrdersCount;
            ViewBag.OnWayOrders = onwayOrders;

            //Trips based statistics
            var tripsTotal = _context.Trips.Count();
            var tripsNotFinished = _context.Trips.Count(t => t.TripFinished == null);
            var tripsFinished = _context.Trips.Count(t => t.TripFinished.HasValue);
            ViewBag.TripTotal = tripsTotal;
            ViewBag.TripsNotFinisged = tripsNotFinished;
            ViewBag.TripsFinished = tripsFinished;

            //Trips with brokers orders
            var tripsWithMyOrders = _context.Trips.Where(t => t.Order.ApplicationUserId == userId).ToList();



            //Populating announcements on dashboard.
            var lastAnonId = _context.Announcments.Max(a => a.Id);
            var lastAnon = _context.Announcments.FirstOrDefault(a => a.Id == lastAnonId);
            var lastAnonAuthor = _context.Users.SingleOrDefault(a => a.Id == lastAnon.ApplicationUserId);
            if (lastAnon == null)
            {
                ViewBag.AnonSubject = "There are no announcements yet";
                ViewBag.AnonMessage = "Ask your boos to announence something!";
                ViewBag.AnonAuthor = "SysAdmin";
                ViewBag.AnonCreatedOn = DateTime.Now;
            }
            else { 
            ViewBag.AnonSubject = lastAnon.Subject;
            ViewBag.AnonMessage = lastAnon.Message;
            ViewBag.AnonAuthor = lastAnonAuthor.FirstName + " " + lastAnonAuthor.LastName;
            ViewBag.AnonCreatedOn = lastAnon.CreatedOn;
            }


            var acceptedOrders = _context.Orders
                .Where(o => o.OrderStatusId == OrderStatus.Accepted)
                .Include(a => a.ApplicationUser)
                .ToList();

            return View(acceptedOrders);
        }

        public int GetOrdersTotal()
        {
            var ordersCount = _context.Orders.Count();
            return ordersCount;
        }

        public int GetBrokersOrdersTotal(string id)
        {
            var brokersOrders = _context.Orders.Where(o => o.ApplicationUserId == id).ToList().Count;

            return brokersOrders;
        }

        public int GetCustomersTotal()
        {
            var customersCount = _context.Customers.Count();
            return customersCount;
        }

         public int GetMostValuedCustomer()
        {
            var mostValuedCustomer = _context.Orders.GroupBy(a => a.CustomerId).Max(k => k.Key);

           return mostValuedCustomer;
        }
      
    }
}