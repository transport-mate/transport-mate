﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Transportation_Company.Models;
using Transportation_Company.ViewModels;
using System.Data.Entity;


namespace Transportation_Company.Controllers
{
    public class CustomersController : Controller
    {



        private ApplicationDbContext _context;

        public CustomersController()
        {
            _context = new ApplicationDbContext();
        }


        // GET: Customers
        [Authorize(Roles = RoleName.Admin + "," + RoleName.Broker)]
        public ViewResult Index()
        {
            //We commented this out because in this particular view, we use api to fill the data on Index page.
           // var customers = _context.Customers.Include(c => c.MembershipType).ToList();
          //  return View(customers);
            return View();
        }

        [Authorize(Roles = RoleName.Admin + "," + RoleName.SafetyManager + "," + RoleName.Broker + "," + RoleName.Driver)]
        public ActionResult Details(int id)
        {
            var customer = _context.Customers.Include(c => c.MembershipType).SingleOrDefault(c => c.Id == id);

            if (customer == null) return HttpNotFound();

            return View(customer);

        }

        [Authorize(Roles = RoleName.Admin + "," + RoleName.Broker)]
        public ActionResult New()
        {
            var memberShipTypes = _context.MembershipTypes.ToList();
            var viewModel = new CustomerViewModel
            {
                MembershipTypes = memberShipTypes
            };
            return View("CustomerForm", viewModel);
        }

        [Authorize(Roles = RoleName.Admin + "," + RoleName.Broker)]
        public ActionResult Edit(int id)
        {
            var customer = _context.Customers.SingleOrDefault(c => c.Id == id);
            if (customer == null) return HttpNotFound();

            var viewModel = new CustomerViewModel
            {
                Customer = customer,
                MembershipTypes = _context.MembershipTypes.ToList()
            };
            return View("CustomerForm", viewModel);

        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = RoleName.Admin + "," + RoleName.Broker)]
        public ActionResult Save(Customer customer)
        {
            if (customer.Id == 0)
                _context.Customers.Add(customer);
            else
            {
                var customerInDb = _context.Customers.Single(c => c.Id == customer.Id);

                customerInDb.Name = customer.Name;
                customerInDb.CompanyName = customer.CompanyName;
                customerInDb.IdentificationNumber = customer.IdentificationNumber;
                customerInDb.Address = customer.Address;
                customerInDb.ZipCode = customer.ZipCode;
                customerInDb.City = customer.City;
                customerInDb.Country = customer.Country;
                customerInDb.Email = customer.Email;
                customerInDb.Comment = customer.Comment;
                customerInDb.PhoneNumber = customer.PhoneNumber;
                customerInDb.PhoneNumberExtension = customer.PhoneNumberExtension;
                customerInDb.BirthDate = customer.BirthDate;
                customerInDb.IsSubscribedToNewsletter = customer.IsSubscribedToNewsletter;
                customerInDb.MembershipTypeId = customer.MembershipTypeId;

            }

            _context.SaveChanges();
            return RedirectToAction("Index", "Customers");
        }

    }
}