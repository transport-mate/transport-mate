﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Transportation_Company.Models;
using System.Data.Entity;
using Transportation_Company.ViewModels;

namespace Transportation_Company.Controllers
{
    [Authorize(Roles = RoleName.Admin + "," + RoleName.SafetyManager)]
    public class ModelMakesController : Controller
    {
        private ApplicationDbContext _context;

        public ModelMakesController()
        {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        // GET: ModelMakes
        public ViewResult Index()
        {
            var models = _context.ModelMakes.Include(c => c.Make).ToList();
            var makes = _context.Makes.ToList();

            var viewModel = new NewPartialModelViewModel
            {
                Makes = makes,
                ModelMakes = models
            };

          

            return View(viewModel);
        }

        public ActionResult Details(int id)
        {
            var modelMake = _context.ModelMakes.SingleOrDefault(m => m.Id == id);

            if (modelMake == null)
                return HttpNotFound();

            return View(modelMake);
        }

        public ActionResult NewModel()
        {
            var makes = _context.Makes.ToList();
            var viewModel = new NewModelViewModel
            {
                Makes = makes
            };
            return View(viewModel);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ModelMake modelMake)
        {
            if (!ModelState.IsValid)
            {
                var viewModel = new NewModelViewModel
                {
                    ModelMake = modelMake,
                    Makes = _context.Makes.ToList()

                };
                return View("NewModel", viewModel);
            }

            modelMake.Name = modelMake.Name.ToUpper();

            _context.ModelMakes.Add(modelMake);
            _context.SaveChanges();
            TempData["Success"] = "Success";
            return RedirectToAction("Index", "ModelMakes");
        }

        public ActionResult Edit(int id)
        {
            var modelMake = _context.ModelMakes.SingleOrDefault(m => m.Id == id);

            if (modelMake == null)
                return HttpNotFound();

            var viewModel = new NewModelViewModel
            {
                ModelMake = modelMake,
                Makes = _context.Makes.ToList()

            };

            return View("EditModel",viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(ModelMake modelMake)
        {
            if (!ModelState.IsValid)
            {
                var viewModel = new NewModelViewModel
                {
                    ModelMake = modelMake,
                    Makes = _context.Makes.ToList()
                };
                return View("EditModel", viewModel);
            }

            var modelInDb = _context.ModelMakes.Single(m => m.Id == modelMake.Id);

            modelInDb.Name = modelMake.Name.ToUpper();
            modelInDb.MakeId = modelMake.MakeId;

            _context.SaveChanges();

            return RedirectToAction("Index", "ModelMakes");
        }
    }
}