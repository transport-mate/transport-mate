﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Transportation_Company.Models;

namespace Transportation_Company.Controllers
{
    [Authorize(Roles = RoleName.Admin)]

    public class AdminController : Controller
    {
        private ApplicationDbContext _context;

        public AdminController()
        {
            _context = new ApplicationDbContext();
        }


        // GET: Admin
        public ActionResult Index()
        {
            //Most valuable customer
            var ordersInDbNumber = _context.Orders.Count();

            if (ordersInDbNumber == 0)
            {
                ViewBag.MostValuedCustomer = "Mr. Nobody";
                ViewBag.MvpOrdersTotal = 0;
            }
            else
            {
                var mostValuedCustomerId = _context.Orders
                    .GroupBy(a => a.CustomerId)
                    .OrderByDescending(g => g.Count())
                    .Take(1)
                    .Select(k => k.Key)
                    .First();
                var mvp = _context.Customers.SingleOrDefault(a => a.Id == mostValuedCustomerId);
                ViewBag.MostValuedCustomer = mvp.CompanyName;
                var mvpOrdersTotal = _context.Orders.Count(a => a.CustomerId == mvp.Id);
                ViewBag.MvpOrdersTotal = mvpOrdersTotal;
            }
      

            //The best broker
            if (ordersInDbNumber == 0)
            {
                ViewBag.TheBestBrokerFullName = "Mr. Nobody";
                ViewBag.NumberOfBestBrokersOrders = 0;
            }
            else
            {
                var theBestBroker = _context.Orders
                    .GroupBy(o => o.ApplicationUserId)
                    .OrderByDescending(g => g.Count())
                    .Take(1)
                    .Select(t => t.Key)
                    .First();
                var theBestBrokerFullName = _context.Users.Single(u => u.Id == theBestBroker);
                var numberOfBestBrokersOrders = _context.Orders.Count(o => o.ApplicationUserId == theBestBroker);
                ViewBag.TheBestBrokerFullName = theBestBrokerFullName.FirstName + " " + theBestBrokerFullName.LastName; ;
                ViewBag.NumberOfBestBrokersOrders = numberOfBestBrokersOrders;
            }
          

            //Vehicles based statistics.
            var trailersCount = _context.Trailers.Count();
            var trucksCount = _context.Trucks.Count();
            var vehiclesCount = trucksCount + trailersCount;
            ViewBag.TrailersCount = trailersCount;
            ViewBag.TrucksCount = trucksCount;
            ViewBag.VehiclesCount = vehiclesCount;


            //Orders based statistics.
            var ordersOverallCount = _context.Orders.Count();
            var deliveredOrders = _context.Orders.Count(o => o.OrderStatusId == OrderStatus.Delivered);
            ViewBag.OrdersOverallCount = ordersOverallCount;
            ViewBag.DeliveredOrders = deliveredOrders;

            //Trips based statistics
            var tripsTotal = _context.Trips.Count();
            var tripsNotFinished = _context.Trips.Count(t => t.TripFinished == null);
            var tripsFinished = _context.Trips.Count(t => t.TripFinished.HasValue);

            var trips = _context.Trips.ToList();

            if (trips.Any())
            {
                var mostUsedTrailerInTrips = _context.Trips
                    .GroupBy(t => t.TrailerId)
                    .OrderByDescending(tp => tp.Count())
                    .Take(1)
                    .Select(g => g.Key)
                    .First();

                var mostUsedTrailerInTripsFullName = _context.Trailers
                    .First(t => t.Id == mostUsedTrailerInTrips);

                ViewBag.MostUsedTrailerInTripsFullName = mostUsedTrailerInTripsFullName.TrailerFullName;

                var topDriverId = _context.Trips
                    .GroupBy(t => t.ApplicationUserId)
                    .OrderByDescending(tp => tp.Count())
                    .Take(1)
                    .Select(t => t.Key)
                    .First();

                var topDriverName = _context.Users.Single(b => b.Id == topDriverId);

                ViewBag.TopDriverName = topDriverName.FullNameWithBirthDate;

            }
        

            ViewBag.TripTotal = tripsTotal;
            ViewBag.TripsNotFinished = tripsNotFinished;
            ViewBag.TripsFinished = tripsFinished;

            //Announcements statistics
            var totalAnnouncements = _context.Announcments.Count();

            if (totalAnnouncements == 0)
            {
                ViewBag.TotalAnnouncements = 0;
                ViewBag.TopAnnouncerName = "Mr. Nobody";
            }
            else
            {
                var topAnnouncer = _context.Announcments
                    .GroupBy(a => a.ApplicationUserId)
                    .OrderByDescending(ap => ap.Count())
                    .Take(1)
                    .Select(a => a.Key)
                    .First();

                var topAnnouncerName = _context.Users.First(u => u.Id == topAnnouncer);

                ViewBag.TotalAnnouncements = totalAnnouncements;
                ViewBag.TopAnnouncerName = topAnnouncerName.FirstName + " " + topAnnouncerName.LastName;
            }
           

            //Blocked users
            var blockedUsersTotal = _context.Users.Count(u => u.IsEnabled == false);
            var activeUserTotal = _context.Users.Count(u => u.IsEnabled == true);
            var users = _context.Users.Count();

            ViewBag.BlockedUsersTotal = blockedUsersTotal;
            ViewBag.ActiveUsersTotal = activeUserTotal;
            ViewBag.UsersTotal = users;

            //Top brand of Truck

            var numberOfBrands = _context.Makes.Count();
            var numberofModels = _context.ModelMakes.Count();

            ViewBag.NumberOfBrands = numberOfBrands;
            ViewBag.NumberOfModels = numberofModels;

            if (trucksCount == 0)
            {
                ViewBag.TopTruckBrandName = "No trucks no brand";

            }
            else
            {
                var topTruckBrand = _context.Trucks
                    .GroupBy(t => t.MakeId)
                    .OrderByDescending(tp => tp.Count())
                    .Take(1)
                    .Select(t => t.Key)
                    .First();

                var topTruckBrandName = _context.Makes.Single(b => b.Id == topTruckBrand);

                ViewBag.TopTruckBrandName = topTruckBrandName.Name;
            }

            //Top brand of trailers

            if (trailersCount == 0)
            {
                ViewBag.TopTrailerBrandName = "No trailers no top brand";
            }
            else
            {
                var topTrailerBrand = _context.Trailers
                    .GroupBy(t => t.MakeId)
                    .OrderByDescending(tp => tp.Count())
                    .Take(1)
                    .Select(t => t.Key)
                    .First();

                var topTrailerBrandName = _context.Makes.Single(b => b.Id == topTrailerBrand);

                ViewBag.TopTrailerBrandName = topTrailerBrandName.Name;
            }
            //Populating announcements on dashboard.
            var lastAnonId = _context.Announcments.Max(a => a.Id);
            var lastAnon = _context.Announcments.FirstOrDefault(a => a.Id == lastAnonId);
            var lastAnonAuthor = _context.Users.SingleOrDefault(a => a.Id == lastAnon.ApplicationUserId);
            if (lastAnon == null)
            {
                ViewBag.AnonSubject = "There are no announcements yet";
                ViewBag.AnonMessage = "Ask your boos to announence something!";
                ViewBag.AnonAuthor = "SysAdmin";
                ViewBag.AnonCreatedOn = DateTime.Now;
            }
            else
            {
                ViewBag.AnonSubject = lastAnon.Subject;
                ViewBag.AnonMessage = lastAnon.Message;
                ViewBag.AnonAuthor = lastAnonAuthor.FirstName + " " + lastAnonAuthor.LastName;
                ViewBag.AnonCreatedOn = lastAnon.CreatedOn;
            }


            var activeUsers = _context.Users.Where(a => a.IsEnabled == true).ToList();
        

            return View(activeUsers);
        }
    }
}