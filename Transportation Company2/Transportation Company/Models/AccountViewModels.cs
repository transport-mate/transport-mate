﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Transportation_Company.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = "Remember this browser?")]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class LoginViewModel
    {
   
       

        [Required]
        [Display(Name = "Email")]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel
    {
        [Required]
        [Display(Name = "User Role")]
        public string UserRoles { get; set; }

       

        [Required]
        [StringLength(255, ErrorMessage = "First name can't exceed 255 characters")]
        [Display(Name = "First name")]
        public string FirstName { get; set; }
        [Required]
        [StringLength(255, ErrorMessage = "Last name can't exceed 255 characters")]
        [Display(Name = "Last name")]
        public string LastName { get; set; }
        [Required]
        [DataType(DataType.Date)]
        [Display(Name = "Date of birth")]
        public DateTime BirthDate { get; set; }
        [Required]
        [StringLength(255, ErrorMessage = "Street name can't exceed 255 characters")]
        [Display(Name = "Street name")]
        public string StreetName { get; set; }
        [Required]
        [Display(Name = "House number")]
        public int HouseNumber { get; set; }
        [StringLength(20, ErrorMessage = "Apartment number can't exceed 20 characters")]
        [Display(Name = "Apartment number")]
        public string AptNumber { get; set; }
        [Required]
        [Display(Name = "Zip Code")]
        [RegularExpression(@"^\b\d{2}-\d{3}\b|\b\d{5}\b$", ErrorMessage = "Invalid Zip Code. The proper Zip Code format is e.g 42567 or 01-008.")]
        public string ZipCode { get; set; }
        [Required]
        [StringLength(255, ErrorMessage = "City name can't exceed 255 characters")]
        public string City { get; set; }
        [StringLength(255, ErrorMessage = "Driving License number can't exceed 255 characters")]
        [Display(Name = "Driving License")]
        public string DrivingLicense { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Registration date")]
        public DateTime DateRegistered { get; set; }

        [Display(Name = "Profile picture")]
        public byte[] ProfilePicture { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class ResetPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }
}
