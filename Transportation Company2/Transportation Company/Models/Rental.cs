﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Transportation_Company.Models
{
    public class Rental
    {
        public int Id { get; set; }

    
        public ApplicationUser ApplicationUser { get; set; }

         [Display(Name = "Driver")]
        public string ApplicationUserId { get; set; }

        
        
        public Truck Truck { get; set; }

        [Display(Name = "Truck")]
        public int TruckId { get; set; }


        [Display(Name = "Rental date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime DateRented { get; set; } = DateTime.Now;

        [Display(Name = "Return date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? DateReturned { get; set; }

    }
}