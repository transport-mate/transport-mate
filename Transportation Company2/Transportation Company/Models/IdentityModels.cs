﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.UI;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Transportation_Company.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        [Required]
        [StringLength(255)]
        [Display(Name = "First name")]
        public string FirstName { get; set; }
        [Required]
        [StringLength(255)]
        [Display(Name = "Last name")]
        public string LastName { get; set; }

        public string FullNameWithBirthDate { get; set; }   


       /* [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public string FullNameWithBirthDate
        {
            get { return FirstName + " " + LastName + ", " + BirthDate.ToShortDateString(); }
           private set { }
        }*/

        [Required]
        [Display(Name = "Date of birth")]
        public DateTime BirthDate { get; set; }
        [Required]
        [StringLength(255)]
        [Display(Name = "Street name")]
        public string StreetName { get; set; }
        [Required]
        [Display(Name = "House number")]
        public int HouseNumber { get; set; }
        [StringLength(20)]
        [Display(Name = "Apartment number")]
        public string AptNumber { get; set; }
        [Required]
        [Display(Name = "Zip Code")]
        [RegularExpression(@"^\b\d{2}-\d{3}\b|\b\d{5}\b$", ErrorMessage = "Invalid Zip Code. The proper Zip Code format is e.g 42567 or 01-008.")]
        public string ZipCode { get; set; }
        [Required]
        [StringLength(255)]
        public string City { get; set; }
        [StringLength(255)]
        [Display(Name = "Driving License")]
        public string DrivingLicense { get; set; }

        [Display(Name = "Profile picture")]
        public byte[] ProfilePicture { get; set; }

        [Display(Name = "Registration date")]
        public DateTime DateRegistered { get; set; } = DateTime.Now;

        [Display(Name = "Is User blocked?")]
        public bool? IsEnabled { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

   /* public class ApplicationRole : IdentityRole
    {
        public ApplicationRole() : base() { }
        public ApplicationRole(string roleName) : base(roleName) { }
    }*/

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {

        public DbSet<Customer> Customers { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderStatus> OrderStatuses { get; set; }
        public DbSet<MembershipType> MembershipTypes { get; set; }
        public DbSet<ShippingType> ShippingTypes { get; set; }

        public DbSet<Rental> Rentals { get; set; }

        public DbSet<TruckMileageHistory> TruckMileageHistory { get; set; } 

        public DbSet<TripStatus> TripStatuses { get; set; }

        public DbSet<Driver> Drivers { get; set; }

        public DbSet<Truck> Trucks { get; set; } 
        public DbSet<Trailer> Trailers { get; set; }

        public DbSet<Make> Makes { get; set; }

        public DbSet<ModelMake> ModelMakes { get; set; }

       // public DbSet<Vehicle> Vehicles { get; set; }
        
        public DbSet<TrailerType> TrailerTypes { get; set; }

        public DbSet<TruckType> TruckTypes { get; set; }

        public DbSet<Trip> Trips { get; set; }

        public DbSet<Announcment> Announcments { get; set; }







        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        // public System.Data.Entity.DbSet<Transportation_Company.Models.ApplicationUser> ApplicationUsers { get; set; }
    }
}
