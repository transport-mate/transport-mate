﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Transportation_Company.Models
{
    public class Announcment
    {
        public int Id { get; set; }
        
        [Required]
        [StringLength(150)]
        public string Subject { get; set; }

        [Required]
        [DataType(DataType.MultilineText)]
        [StringLength(255)]
        public string Message { get; set; }


        [DataType(DataType.DateTime)]
        [Display(Name = "Announced")]
        public DateTime CreatedOn { get; set; } = DateTime.Now;

        public ApplicationUser ApplicationUser { get; set; }

        [Display(Name = "Author")]
        public string ApplicationUserId { get; set; }


    }
}