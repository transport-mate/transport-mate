﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Transportation_Company.Models
{
    public class ShippingType
    {
        public int Id { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [Required]
        [Range(1,10000)]
        public int Rate { get; set; }

        [Required]
        [Range(1,250)]
        public byte Duration { get; set; }
    }
}