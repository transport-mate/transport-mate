﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Transportation_Company.Models
{
    public class TruckMileageHistory
    {
        public int Id { get; set; }

        public DateTime DateEntered { get; set; } = DateTime.Now;

        public int Mileage { get; set; }

        public Truck Truck { get; set; }


        [Display(Name = "Truck")]
        public int TruckId { get; set; }
    }
}