﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Transportation_Company.Models
{
    public class Trip
    {
        public int Id { get; set; }

        [Display(Name = "Trip started")]
        public DateTime TripStarted { get; set; } = DateTime.Now;

        [Display(Name = "Trip finished date")]
        public DateTime? TripFinished { get; set; }

        public Trailer Trailer { get; set; }

        [Display(Name = "Trailer")]
        public int TrailerId { get; set; }

        public ApplicationUser ApplicationUser { get; set; }

        [Display(Name = "Driver")]
        public string ApplicationUserId { get; set; }

        public Order Order { get; set; }

        [Display(Name = "Order")]
        public long OrderId { get; set; }


        public TripStatus TripStatus { get; set; }

        [Display(Name = "Trip status")]
        public byte TripStatusId { get; set; } = 1;
    }
}