﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Transportation_Company.Models
{
    public class TripStatus
    {
        public byte Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public static readonly byte Created = 1;
        public static readonly byte Ready = 2;
        public static readonly byte Picked = 3;
        public static readonly byte Switched = 4;
        public static readonly byte Delivered = 5;


    }
}