﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Transportation_Company.Models
{
    public class Trailer : Vehicle
    {
        [Required]
        [Display(Name = "Length [ft]")]
        public int Length { get; set; }

        [Required]
        [Display(Name = "Capacity [m3]")]
        public int Capacity { get; set; }

        [Display(Name = "Available?")]
        public bool IsAvailable { get; set; } = true;

        public int NumberInStock { get; set; } = 1;

        [Display(Name = "Maintenance?")]
        public bool NeedsMaintenance { get; set; } = false;

        [Display(Name = "Is it taken?")]
        public bool IsTaken { get; set; } = false;

      //  [Required]
       // public Truck Truck { get; set; }

        public TrailerType TrailerType { get; set; }

        [Display(Name = "Trailer type")]
        public int TrailerTypeId { get; set; }

        public Make Make { get; set; }

        [Display(Name = "Brand/Make")]
        public int MakeId { get; set; }


        public ModelMake ModelMake { get; set; }

        [Display(Name = "Model")]
        public int ModelMakeId { get; set; }


        public string TrailerFullName { get; set; }
    }
}