﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Transportation_Company.Models
{
    public class OrderStatus
    {
        public byte Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public static readonly byte Accepted = 1;
        public static readonly byte Assigned = 2;
        public static readonly byte Cancelled = 3;
        public static readonly byte PickedUp = 4;
        public static readonly byte Delayed = 5;
        public static readonly byte Delivered = 6;
    }
}