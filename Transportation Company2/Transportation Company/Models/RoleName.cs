﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Transportation_Company.Models
{
    public static class RoleName
    {
        public const string Admin = "Admin";
        public const string Broker = "Broker";
        public const string SafetyManager = "SafetyManager";
        public const string Driver = "Driver";

    }
}