﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Transportation_Company.Models
{
    public class Vehicle
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "Licence plate")]
        public string LicencePlate { get; set; }

       /* [Required]
        public string Brand { get; set; }

        [Required]
        public string Model { get; set; }*/

        [Required]
        [Display(Name = "Year of production")]
        [Range(1950, 2100, ErrorMessage = "Please enter a valid production year!")]
        public int YearOfProduction { get; set; }

        public int Age()
        {
            return DateTime.Now.Year - YearOfProduction;
        }
    }
}