﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Transportation_Company.Models
{
    public class Driver
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "License Number")]
        public String LicenseNumber { get; set; }
       
    }
}