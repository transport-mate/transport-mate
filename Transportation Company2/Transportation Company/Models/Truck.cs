﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Transportation_Company.Models
{
    public class Truck : Vehicle
    {
        [DisplayFormat(DataFormatString = "{0:G29}", ApplyFormatInEditMode = true)]
        public decimal Engine { get; set; }

        [Required]
        [Display(Name = "Lifting capacity [m3]")]
        public int LiftingCapacity { get; set; }

        // public Trailer Trailer { get; set; }

        //  public Driver Driver { get; set; }

        [Display(Name = "Available?")]
        public bool IsAvailable { get; set; } = true;

        [Display(Name = "Number in stock")]
        public int NumberInStock { get; set; } = 1;

        public byte NumberAvailable { get; set; } = 1;

        [Display(Name = "Maintenance?")]
        public bool NeedsMaintenance { get; set; } = false;

        [Display(Name = "Is it taken?")]
        public bool IsTaken { get; set; } = false;

        public TruckType TruckType { get; set; }

        [Display(Name = "Truck type")]
        public int TruckTypeId { get; set; }

        public Make Make { get; set; }

        [Display(Name = "Brand/Make")]
        public int MakeId { get; set; }


        public ModelMake ModelMake { get; set; }

        [Display(Name = "Model")]
        public int ModelMakeId { get; set; }


        public string TruckFullName { get; set; }
    }
}