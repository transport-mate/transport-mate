﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.EnterpriseServices;
using System.Linq;
using System.Web;

namespace Transportation_Company.Models
{
    public class Order
    {

        public long Id { get; set; }


        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Creation date")]
        public DateTime CreatedOnDate { get; set; } = DateTime.Now;

        [Display(Name = "Tracking number")]
        public Guid TrackingNumber { get; set; } = Guid.NewGuid();

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Date of pick up")]
        public DateTime PickUpDate { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Date of delivery")]
        public DateTime DeliveryDate { get; set; }

        [Display(Name = "Pick up location")]
        public string PickUpLocation { get; set; }

        [Display(Name = "Delivery location")]
        public string DeliveryLocation { get; set; }

        [Display(Name = "Distance between locations [km]")]
        public int Distance { get; set; }

        [Display(Name = "Pick up #")]
        public string PickUpNumber { get; set; }

        [Display(Name = "Delivery #")]
        public string DeliveryNumber { get; set; }

        [Display(Name = "Weight [kg]")]
        public double Weight { get; set; }

        [Display(Name = "Width [cm]")]
        public double Width { get; set; }

        [Display(Name = "Height [cm]")]
        public double Height { get; set; }

        [Display(Name = "Length [cm]")]
        public double Length { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public double Volume
        {
            get { return Width * Height * Length/1000; }
            private set { }
        } 

        [Display(Name = "Is the order for refrigerated trailer?")]
        public bool IsRefrigerated { get; set; }

        [Display(Name = "Minimum temperature")]
        public float? MinTemperature { get; set; }

        [Display(Name = "Maximum temperature")]
        public float? MaxTemperature { get; set; }

        public ShippingType ShippingType { get; set; }

        [Display(Name = "Shipping type")]
        public int ShippingTypeId { get; set; }
        public Customer Customer { get; set; }

        [Display(Name = "Customer")]
        public int CustomerId { get; set; }

        public OrderStatus OrderStatus { get; set; }

        [Display(Name = "Order status")]
        public byte OrderStatusId { get; set; } = OrderStatus.Accepted;

        public ApplicationUser ApplicationUser { get; set; }

        [Display(Name = "Author")]
        public string ApplicationUserId { get; set; }

        [Display(Name = "Additional information")]
        [StringLength(1500)]
        public string AdditionalInformation { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public string FullNaming {
            get
            {
                return Id.ToString() + ", " + PickUpLocation + " -> " + DeliveryLocation + " p:" + PickUpDate + " d:" +
                       DeliveryDate;
            }
            private set {} }  



        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
       public double Price
        {
           get
           {
               double priceForWeight = PricePerKg * Weight;
               double priceForDistance = PricePerDistance * Distance;
               double priceForVolume = PricePerVolume * Volume;
               double fullPrice = priceForDistance + priceForWeight + priceForVolume;
               return fullPrice;
           }
           private set
           {
               
           }
        }

                private double PricePerDistance = 0.5; 
                private double PricePerKg = 10;
                private double PricePerVolume = 100;
    }
    }

    
