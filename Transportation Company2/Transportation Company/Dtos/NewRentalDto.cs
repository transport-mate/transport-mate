﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Transportation_Company.Dtos
{
    public class NewRentalDto
    {
        public string ApplicationUserId { get; set; }

        public List<int> TruckIds { get; set; }

    }
}