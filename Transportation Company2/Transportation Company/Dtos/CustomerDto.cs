﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Transportation_Company.Models;

namespace Transportation_Company.Dtos
{
    public class CustomerDto
    {
        public int Id { get; set; }


        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        [StringLength(255)]
        public string CompanyName { get; set; }

        [Required]
        [RegularExpression(@"^\b\d{3}-\d{3}-\d{2}-\d{2}\b|\b\d{3}-\d{2}-\d{2}-\d{3}\b|\b\d{2}-\d{7}\b$", ErrorMessage = "Invalid identification number. Match the pattern e.g 22-4456432.")]
        public string IdentificationNumber { get; set; }


        [StringLength(255)]
        public string Address { get; set; }

        [StringLength(6)]
        [RegularExpression(@"^\b\d{2}-\d{3}\b|\b\d{5}\b$", ErrorMessage = "Invalid Zip Code. The proper Zip Code format is e.g 42567 or 01-008.")]
        public string ZipCode { get; set; }

        [StringLength(50)]
        public string City { get; set; }

        [StringLength(50)]
        public string Country { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-.●]?([0-9]{3})[-.●]?([0-9]{4})$", ErrorMessage = "Phone number is not valid.")]
        public string PhoneNumber { get; set; }


        [RegularExpression(@"^(x\d{4})|(x\d{3})|(x\d{2})$", ErrorMessage = "This is not a valid extension phone number ")]
        public string PhoneNumberExtension { get; set; }



        public DateTime? BirthDate { get; set; }

        public bool IsSubscribedToNewsletter { get; set; }

        [StringLength(1200)]
        public string Comment { get; set; }

        public MembershipTypeDto MembershipType { get; set; }

        public byte MembershipTypeId { get; set; }

    }
}