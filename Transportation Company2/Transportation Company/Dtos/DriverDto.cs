﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Transportation_Company.Dtos
{
    public class DriverDto
    {
        public int Id { get; set; }

        [Required]
        public String LicenseNumber { get; set; }

    }
}