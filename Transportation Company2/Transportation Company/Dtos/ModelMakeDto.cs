﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Transportation_Company.Models;

namespace Transportation_Company.Dtos
{
    public class ModelMakeDto
    {
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }


        public int MakeId { get; set; }

        public MakeDto Make { get; set; }
    }
}