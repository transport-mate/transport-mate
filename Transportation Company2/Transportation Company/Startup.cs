﻿using System;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Owin;
using Transportation_Company.Models;

[assembly: OwinStartupAttribute(typeof(Transportation_Company.Startup))]
namespace Transportation_Company
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            createRolesandUsers();
        }

        // In this method we will create default User roles and Admin user for login    
        private void createRolesandUsers()
        {
            ApplicationDbContext context = new ApplicationDbContext();

            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));


            // In Startup iam creating first Admin Role     
            if (!roleManager.RoleExists("Admin"))
            {

                //create Admin role    
                var role = new Microsoft.AspNet.Identity.EntityFramework.IdentityRole();
                role.Name = "Admin";
                roleManager.Create(role);

                //Here we create a super user who will maintain the website. 
                var user = new ApplicationUser();
               
                user.Email = "s12824@pjwstk.edu.pl";
                user.FirstName = "SUPERUSER";
                user.LastName = "SUPERUSER";
                user.BirthDate = new DateTime(2001-01-01);
                user.StreetName = "SUPERUSER";
                user.HouseNumber = 4;
                user.ZipCode = "02-008";
                user.City = "SUPERUSER";
                user.IsEnabled = true;
                user.DateRegistered = DateTime.Now;
                user.FullNameWithBirthDate =
                    user.FirstName + ", " + user.LastName + ", " + user.BirthDate.ToShortDateString();
                user.DrivingLicense = "AXB123";
                string userPassword = "A@Z200711";
                var chkUser = UserManager.Create(user, userPassword);

                // Add default user to role admin
                if (chkUser.Succeeded)
                {
                    var result1 = UserManager.AddToRole(user.Id, "Admin");
                }
            }

            // creating Broker role     
            if (!roleManager.RoleExists("Broker"))
            {
                var role = new Microsoft.AspNet.Identity.EntityFramework.IdentityRole();
                role.Name = "Broker";
                roleManager.Create(role);

            }

            // creating Safety Manager role     
            if (!roleManager.RoleExists("SafetyManager"))
            {
                var role = new Microsoft.AspNet.Identity.EntityFramework.IdentityRole();
                role.Name = "SafetyManager";
                roleManager.Create(role);

            }

            // creating Driver role     
            if (!roleManager.RoleExists("Driver"))
            {
                var role = new Microsoft.AspNet.Identity.EntityFramework.IdentityRole();
                role.Name = "Driver";
                roleManager.Create(role);

            }

         }
    }
}
