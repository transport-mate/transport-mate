namespace Transportation_Company.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IsAvailableAttributeAddedToTruckClass : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Trucks", "IsAvailable", c => c.Boolean(nullable: false));

        }
        
        public override void Down()
        {
            DropColumn("dbo.Trucks", "IsAvailable");
        }
    }
}
