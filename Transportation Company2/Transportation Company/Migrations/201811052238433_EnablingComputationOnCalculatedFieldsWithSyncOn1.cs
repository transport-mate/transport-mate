namespace Transportation_Company.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EnablingComputationOnCalculatedFieldsWithSyncOn1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "Volume", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Orders", "Volume");
        }
    }
}
