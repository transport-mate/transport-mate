namespace Transportation_Company.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FixedCustomerModel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Customers", "IdentificationNumber", c => c.String(nullable: false));
            AddColumn("dbo.Customers", "Address", c => c.String(maxLength: 255));
            AddColumn("dbo.Customers", "ZipCode", c => c.String(maxLength: 6));
            AddColumn("dbo.Customers", "City", c => c.String(maxLength: 50));
            AddColumn("dbo.Customers", "Country", c => c.String(maxLength: 50));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Customers", "Country");
            DropColumn("dbo.Customers", "City");
            DropColumn("dbo.Customers", "ZipCode");
            DropColumn("dbo.Customers", "Address");
            DropColumn("dbo.Customers", "IdentificationNumber");
        }
    }
}
