namespace Transportation_Company.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateModelsOrderAndShippingType : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Orders", "ShippingType_Id", "dbo.ShippingTypes");
            DropIndex("dbo.Orders", new[] { "ShippingType_Id" });
            RenameColumn(table: "dbo.Orders", name: "ShippingType_Id", newName: "ShippingTypeId");
            DropPrimaryKey("dbo.ShippingTypes");
            AlterColumn("dbo.Orders", "ShippingTypeId", c => c.Int(nullable: false));
            AlterColumn("dbo.ShippingTypes", "Id", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.ShippingTypes", "Id");
            CreateIndex("dbo.Orders", "ShippingTypeId");
            AddForeignKey("dbo.Orders", "ShippingTypeId", "dbo.ShippingTypes", "Id", cascadeDelete: true);
            DropColumn("dbo.Orders", "ShippingId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Orders", "ShippingId", c => c.Int(nullable: false));
            DropForeignKey("dbo.Orders", "ShippingTypeId", "dbo.ShippingTypes");
            DropIndex("dbo.Orders", new[] { "ShippingTypeId" });
            DropPrimaryKey("dbo.ShippingTypes");
            AlterColumn("dbo.ShippingTypes", "Id", c => c.Long(nullable: false, identity: true));
            AlterColumn("dbo.Orders", "ShippingTypeId", c => c.Long());
            AddPrimaryKey("dbo.ShippingTypes", "Id");
            RenameColumn(table: "dbo.Orders", name: "ShippingTypeId", newName: "ShippingType_Id");
            CreateIndex("dbo.Orders", "ShippingType_Id");
            AddForeignKey("dbo.Orders", "ShippingType_Id", "dbo.ShippingTypes", "Id");
        }
    }
}
