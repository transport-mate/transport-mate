namespace Transportation_Company.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangingVehiclesAttributes : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Trucks", "Brand");
            DropColumn("dbo.Trucks", "Model");
            DropColumn("dbo.Trailers", "Brand");
            DropColumn("dbo.Trailers", "Model");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Trailers", "Model", c => c.String(nullable: false));
            AddColumn("dbo.Trailers", "Brand", c => c.String(nullable: false));
            AddColumn("dbo.Trucks", "Model", c => c.String(nullable: false));
            AddColumn("dbo.Trucks", "Brand", c => c.String(nullable: false));
        }
    }
}
