namespace Transportation_Company.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateTrailerandTruckTypesDataAnnotations : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.TruckTypes", "Name", c => c.String(nullable: false, maxLength: 200));
            AlterColumn("dbo.TruckTypes", "Description", c => c.String(nullable: false, maxLength: 255));
            AlterColumn("dbo.TrailerTypes", "Name", c => c.String(nullable: false, maxLength: 200));
            AlterColumn("dbo.TrailerTypes", "Description", c => c.String(nullable: false, maxLength: 255));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.TrailerTypes", "Description", c => c.String(nullable: false));
            AlterColumn("dbo.TrailerTypes", "Name", c => c.String(nullable: false));
            AlterColumn("dbo.TruckTypes", "Description", c => c.String(nullable: false));
            AlterColumn("dbo.TruckTypes", "Name", c => c.String(nullable: false));
        }
    }
}
