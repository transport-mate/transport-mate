namespace Transportation_Company.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AdditionalMigrationAsDEbuggerSuggests : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Trucks", "Driver_Id", "dbo.Drivers");
            DropForeignKey("dbo.Trailers", "Id", "dbo.Trucks");
            DropIndex("dbo.Trucks", new[] { "Driver_Id" });
            DropIndex("dbo.Trailers", new[] { "Id" });
            CreateTable(
                "dbo.TruckTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Description = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TrailerTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Description = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Trucks", "TruckTypeId", c => c.Int(nullable: false));
            CreateIndex("dbo.Trucks", "TruckTypeId");
            AddForeignKey("dbo.Trucks", "TruckTypeId", "dbo.TruckTypes", "Id", cascadeDelete: true);
            DropColumn("dbo.Trucks", "Driver_Id");
            DropTable("dbo.Trailers");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Trailers",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Length = c.Int(nullable: false),
                        Capacity = c.Int(nullable: false),
                        LicencePlate = c.String(nullable: false),
                        Brand = c.String(nullable: false),
                        Model = c.String(nullable: false),
                        YearOfProduction = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Trucks", "Driver_Id", c => c.Int());
            DropForeignKey("dbo.Trucks", "TruckTypeId", "dbo.TruckTypes");
            DropIndex("dbo.Trucks", new[] { "TruckTypeId" });
            DropColumn("dbo.Trucks", "TruckTypeId");
            DropTable("dbo.TrailerTypes");
            DropTable("dbo.TruckTypes");
            CreateIndex("dbo.Trailers", "Id");
            CreateIndex("dbo.Trucks", "Driver_Id");
            AddForeignKey("dbo.Trailers", "Id", "dbo.Trucks", "Id");
            AddForeignKey("dbo.Trucks", "Driver_Id", "dbo.Drivers", "Id");
        }
    }
}
