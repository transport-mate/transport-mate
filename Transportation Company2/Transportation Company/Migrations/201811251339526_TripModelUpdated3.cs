namespace Transportation_Company.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TripModelUpdated3 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Trips", "OrderId", c => c.Long(nullable: false));
            CreateIndex("dbo.Trips", "OrderId");
            AddForeignKey("dbo.Trips", "OrderId", "dbo.Orders", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Trips", "OrderId", "dbo.Orders");
            DropIndex("dbo.Trips", new[] { "OrderId" });
            DropColumn("dbo.Trips", "OrderId");
        }
    }
}
