namespace Transportation_Company.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangedPropertiesofTruckClass : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Trucks", "NumberInStock", c => c.Int(nullable: false));
            AddColumn("dbo.Trucks", "NeedsMaintenance", c => c.Boolean(nullable: false));
            AddColumn("dbo.Trucks", "MakeId", c => c.Int(nullable: false));
            AddColumn("dbo.Trucks", "ModelMakeId", c => c.Int(nullable: false));
            CreateIndex("dbo.Trucks", "MakeId");
            CreateIndex("dbo.Trucks", "ModelMakeId");
            AddForeignKey("dbo.Trucks", "MakeId", "dbo.Makes", "Id", cascadeDelete: false);
            AddForeignKey("dbo.Trucks", "ModelMakeId", "dbo.ModelMakes", "Id", cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Trucks", "ModelMakeId", "dbo.ModelMakes");
            DropForeignKey("dbo.Trucks", "MakeId", "dbo.Makes");
            DropIndex("dbo.Trucks", new[] { "ModelMakeId" });
            DropIndex("dbo.Trucks", new[] { "MakeId" });
            DropColumn("dbo.Trucks", "ModelMakeId");
            DropColumn("dbo.Trucks", "MakeId");
            DropColumn("dbo.Trucks", "NeedsMaintenance");
            DropColumn("dbo.Trucks", "NumberInStock");
        }
    }
}
