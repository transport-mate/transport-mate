namespace Transportation_Company.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SeedUsers : DbMigration
    {
        public override void Up()
        {
            Sql(@"
                    INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'96f21686-22c2-4181-91f3-1fbb0aca3249', N'admin1@transportmate.com', 0, N'AMNSSykgUiEFjtAyOFMcmBpqN/CG9trPR4w/JclJmvUEOj/wgmqgwvzKwAtLV1lFoA==', N'b72bfff3-c232-4cde-b5e2-391aa4a6e1cd', NULL, 0, 0, NULL, 1, 0, N'admin1@transportmate.com')
                    INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'cf1a75ac-567e-440e-862c-bfc9c06351f6', N'guest@transportmate.com', 0, N'AC4jblPEeYzbDJrwGilfLu53WcUGE8eX/5ENIMqCnCOKwnUxDqyb5btVIbpXXg7UFA==', N'79c8106a-d4fe-4948-a0cf-481562d7f663', NULL, 0, 0, NULL, 1, 0, N'guest@transportmate.com')
                    
                    INSERT INTO [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'c5cdb4b2-85b3-4917-abf9-d30a2d3fd784', N'CanManageCustomers')

                    INSERT INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'96f21686-22c2-4181-91f3-1fbb0aca3249', N'c5cdb4b2-85b3-4917-abf9-d30a2d3fd784')
                ");
        }
        
        public override void Down()
        {
        }
    }
}
