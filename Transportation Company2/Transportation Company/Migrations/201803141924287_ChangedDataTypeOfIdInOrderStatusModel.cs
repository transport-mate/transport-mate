namespace Transportation_Company.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangedDataTypeOfIdInOrderStatusModel : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Orders", "OrderStatus_Id", "dbo.OrderStatus");
            DropIndex("dbo.Orders", new[] { "OrderStatus_Id" });
            DropPrimaryKey("dbo.OrderStatus");
            AlterColumn("dbo.Orders", "OrderStatus_Id", c => c.Byte());
            AlterColumn("dbo.OrderStatus", "Id", c => c.Byte(nullable: false));
            AddPrimaryKey("dbo.OrderStatus", "Id");
            CreateIndex("dbo.Orders", "OrderStatus_Id");
            AddForeignKey("dbo.Orders", "OrderStatus_Id", "dbo.OrderStatus", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Orders", "OrderStatus_Id", "dbo.OrderStatus");
            DropIndex("dbo.Orders", new[] { "OrderStatus_Id" });
            DropPrimaryKey("dbo.OrderStatus");
            AlterColumn("dbo.OrderStatus", "Id", c => c.Long(nullable: false, identity: true));
            AlterColumn("dbo.Orders", "OrderStatus_Id", c => c.Long());
            AddPrimaryKey("dbo.OrderStatus", "Id");
            CreateIndex("dbo.Orders", "OrderStatus_Id");
            AddForeignKey("dbo.Orders", "OrderStatus_Id", "dbo.OrderStatus", "Id");
        }
    }
}
