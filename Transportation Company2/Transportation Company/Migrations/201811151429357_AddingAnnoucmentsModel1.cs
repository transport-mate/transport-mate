namespace Transportation_Company.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddingAnnoucmentsModel1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Announcments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Subject = c.String(nullable: false, maxLength: 150),
                        Message = c.String(nullable: false, maxLength: 255),
                        CreatedOn = c.DateTime(nullable: false),
                        ApplicationUserId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.ApplicationUserId)
                .Index(t => t.ApplicationUserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Announcments", "ApplicationUserId", "dbo.AspNetUsers");
            DropIndex("dbo.Announcments", new[] { "ApplicationUserId" });
            DropTable("dbo.Announcments");
        }
    }
}
