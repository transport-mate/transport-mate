namespace Transportation_Company.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangedDataTypeOfEnginePropertyInTruckModel : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Trucks", "Engine", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Trucks", "Engine", c => c.Single(nullable: false));
        }
    }
}
