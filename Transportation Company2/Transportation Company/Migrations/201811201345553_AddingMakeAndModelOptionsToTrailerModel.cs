namespace Transportation_Company.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddingMakeAndModelOptionsToTrailerModel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Trailers", "MakeId", c => c.Int(nullable: false));
            AddColumn("dbo.Trailers", "ModelMakeId", c => c.Int(nullable: false));
            CreateIndex("dbo.Trailers", "MakeId");
            CreateIndex("dbo.Trailers", "ModelMakeId");
            AddForeignKey("dbo.Trailers", "MakeId", "dbo.Makes", "Id", cascadeDelete: false);
            AddForeignKey("dbo.Trailers", "ModelMakeId", "dbo.ModelMakes", "Id", cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Trailers", "ModelMakeId", "dbo.ModelMakes");
            DropForeignKey("dbo.Trailers", "MakeId", "dbo.Makes");
            DropIndex("dbo.Trailers", new[] { "ModelMakeId" });
            DropIndex("dbo.Trailers", new[] { "MakeId" });
            DropColumn("dbo.Trailers", "ModelMakeId");
            DropColumn("dbo.Trailers", "MakeId");
        }
    }
}
