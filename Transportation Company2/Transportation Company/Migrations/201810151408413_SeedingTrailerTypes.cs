namespace Transportation_Company.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SeedingTrailerTypes : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO TrailerTypes(Name, Description) VALUES ('Dry','Trailer is for dry load only like Snacks or Tires')");
            Sql("INSERT INTO TrailerTypes(Name, Description) VALUES ('Refrigerated','Trailer is for load like Vegetables or Glued Stickers')");
        }
        
        public override void Down()
        {
        }
    }
}
