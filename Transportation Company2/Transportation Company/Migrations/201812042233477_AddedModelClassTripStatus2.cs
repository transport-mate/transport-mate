namespace Transportation_Company.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedModelClassTripStatus2 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TripStatus",
                c => new
                    {
                        Id = c.Byte(nullable: false),
                        Name = c.String(),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.TripStatus");
        }
    }
}
