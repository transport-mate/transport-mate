namespace Transportation_Company.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SeedingShippingTypes : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO ShippingTypes (Name, Rate, Duration) VALUES ('Air Economy', 3000, 7)");
            Sql("INSERT INTO ShippingTypes (Name, Rate, Duration) VALUES ('Air Priority', 5000, 3)");
            Sql("INSERT INTO ShippingTypes (Name, Rate, Duration) VALUES ('Air Express', 7000, 1)");
            Sql("INSERT INTO ShippingTypes (Name, Rate, Duration) VALUES ('Ground Economy', 750, 7)");
            Sql("INSERT INTO ShippingTypes (Name, Rate, Duration) VALUES ('Ground Priority', 1500, 5)");
            Sql("INSERT INTO ShippingTypes (Name, Rate, Duration) VALUES ('Ground Express', 2500, 2)");
            Sql("INSERT INTO ShippingTypes (Name, Rate, Duration) VALUES ('Sea Economy', 500, 120)");
            Sql("INSERT INTO ShippingTypes (Name, Rate, Duration) VALUES ('Sea Priority', 1250, 28)");
    
        }
        
        public override void Down()
        {
        }
    }
}
