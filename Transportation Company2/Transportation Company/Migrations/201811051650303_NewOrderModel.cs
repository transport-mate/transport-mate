namespace Transportation_Company.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NewOrderModel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "Weight", c => c.Double(nullable: false));
            AddColumn("dbo.Orders", "Width", c => c.Double(nullable: false));
            AddColumn("dbo.Orders", "Height", c => c.Double(nullable: false));
            AddColumn("dbo.Orders", "Length", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Orders", "Length");
            DropColumn("dbo.Orders", "Height");
            DropColumn("dbo.Orders", "Width");
            DropColumn("dbo.Orders", "Weight");
        }
    }
}
