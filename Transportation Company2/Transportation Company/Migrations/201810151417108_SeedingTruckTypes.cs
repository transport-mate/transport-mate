namespace Transportation_Company.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SeedingTruckTypes : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO TruckTypes(Name, Description) VALUES ('Sleeper','Truck with space desired for sleep')");
            Sql("INSERT INTO TruckTypes(Name, Description) VALUES ('Daycab','Shorter truck for city only routes')");
            Sql("INSERT INTO TruckTypes(Name, Description) VALUES ('Strait','Truck with cargo space attached')");

        }

        public override void Down()
        {
        }
    }
}
