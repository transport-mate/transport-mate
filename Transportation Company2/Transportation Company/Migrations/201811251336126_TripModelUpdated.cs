namespace Transportation_Company.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TripModelUpdated : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Trips", "DriverId", "dbo.Drivers");
            DropForeignKey("dbo.Trips", "Order_Id", "dbo.Orders");
            DropIndex("dbo.Trips", new[] { "DriverId" });
            DropIndex("dbo.Trips", new[] { "Order_Id" });
            RenameColumn(table: "dbo.Trips", name: "Trailer_Id", newName: "TrailerId");
            RenameIndex(table: "dbo.Trips", name: "IX_Trailer_Id", newName: "IX_TrailerId");
            AddColumn("dbo.Trips", "TripStarted", c => c.DateTime(nullable: false));
            AddColumn("dbo.Trips", "TripFinished", c => c.DateTime());
            AddColumn("dbo.Trips", "ApplicationUserId", c => c.String(maxLength: 128));
            AlterColumn("dbo.Trips", "Order_Id", c => c.Long());
            CreateIndex("dbo.Trips", "ApplicationUserId");
            CreateIndex("dbo.Trips", "Order_Id");
            AddForeignKey("dbo.Trips", "ApplicationUserId", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.Trips", "Order_Id", "dbo.Orders", "Id");
            DropColumn("dbo.Trips", "DriverId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Trips", "DriverId", c => c.Int(nullable: false));
            DropForeignKey("dbo.Trips", "Order_Id", "dbo.Orders");
            DropForeignKey("dbo.Trips", "ApplicationUserId", "dbo.AspNetUsers");
            DropIndex("dbo.Trips", new[] { "Order_Id" });
            DropIndex("dbo.Trips", new[] { "ApplicationUserId" });
            AlterColumn("dbo.Trips", "Order_Id", c => c.Long(nullable: false));
            DropColumn("dbo.Trips", "ApplicationUserId");
            DropColumn("dbo.Trips", "TripFinished");
            DropColumn("dbo.Trips", "TripStarted");
            RenameIndex(table: "dbo.Trips", name: "IX_TrailerId", newName: "IX_Trailer_Id");
            RenameColumn(table: "dbo.Trips", name: "TrailerId", newName: "Trailer_Id");
            CreateIndex("dbo.Trips", "Order_Id");
            CreateIndex("dbo.Trips", "DriverId");
            AddForeignKey("dbo.Trips", "Order_Id", "dbo.Orders", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Trips", "DriverId", "dbo.Drivers", "Id", cascadeDelete: true);
        }
    }
}
