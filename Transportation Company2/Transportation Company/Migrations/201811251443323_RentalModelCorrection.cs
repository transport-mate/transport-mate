namespace Transportation_Company.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RentalModelCorrection : DbMigration
    {
        public override void Up()
        {
           
            DropIndex("dbo.Rentals", new[] { "DriverId" });
            DropForeignKey("dbo.Rentals", "DriverId", "dbo.Drivers");
           
            AddColumn("dbo.Rentals", "ApplicationUser_Id", c => c.String(nullable: false, maxLength: 128));
            CreateIndex("dbo.Rentals", "ApplicationUser_Id");
            AddForeignKey("dbo.Rentals", "ApplicationUser_Id", "dbo.AspNetUsers", "Id", cascadeDelete: true);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Rentals", "ApplicationUser_Id", "dbo.AspNetUsers");
            DropIndex("dbo.Rentals", new[] { "ApplicationUser_Id" });
            DropColumn("dbo.Rentals", "ApplicationUser_Id");
            
            AddForeignKey("dbo.Rentals", "DriverId", "dbo.Drivers", "Id", cascadeDelete: true);
            CreateIndex("dbo.Rentals", "DriverId");

           
        }
    }
}
