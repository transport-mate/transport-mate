namespace Transportation_Company.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FixedOrderModel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "PickUpDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Orders", "DeliveryDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Orders", "PickUpLocation", c => c.String());
            AddColumn("dbo.Orders", "DeliveryLocation", c => c.String());
            AddColumn("dbo.Orders", "Distance", c => c.Int(nullable: false));
            AddColumn("dbo.Orders", "ShippingId", c => c.Int(nullable: false));
            AddColumn("dbo.Orders", "CustomerId", c => c.Int(nullable: false));
            CreateIndex("dbo.Orders", "CustomerId");
            AddForeignKey("dbo.Orders", "CustomerId", "dbo.Customers", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Orders", "CustomerId", "dbo.Customers");
            DropIndex("dbo.Orders", new[] { "CustomerId" });
            DropColumn("dbo.Orders", "CustomerId");
            DropColumn("dbo.Orders", "ShippingId");
            DropColumn("dbo.Orders", "Distance");
            DropColumn("dbo.Orders", "DeliveryLocation");
            DropColumn("dbo.Orders", "PickUpLocation");
            DropColumn("dbo.Orders", "DeliveryDate");
            DropColumn("dbo.Orders", "PickUpDate");
        }
    }
}
