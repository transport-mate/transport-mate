namespace Transportation_Company.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SeedTripStatuses : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO TripStatus (Id, Name, Description) VALUES (1, 'Created', 'Set automatically when new Trip is created.')");
            Sql("INSERT INTO TripStatus (Id, Name, Description) VALUES (2, 'Ready', 'Set manuallyy when Driver starts to drive')");
            Sql("INSERT INTO TripStatus (Id, Name, Description) VALUES (3, 'Picked', 'Set manually when Driver pick the package')");
            Sql("INSERT INTO TripStatus (Id, Name, Description) VALUES (4, 'Switched', 'Set manually by Broker when there is a drop and hook.')");
            Sql("INSERT INTO TripStatus (Id, Name, Description) VALUES (5, 'Delivered', 'Set manually by Driver when package is delivered.')");

        }

        public override void Down()
        {
        }
    }
}
