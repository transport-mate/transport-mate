namespace Transportation_Company.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ResettingRentalModel2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Rentals", "ApplicationUserId", c => c.String(maxLength: 128));
            AddColumn("dbo.Rentals", "TruckId", c => c.Int(nullable: false));
            CreateIndex("dbo.Rentals", "ApplicationUserId");
            CreateIndex("dbo.Rentals", "TruckId");
            AddForeignKey("dbo.Rentals", "ApplicationUserId", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.Rentals", "TruckId", "dbo.Trucks", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Rentals", "TruckId", "dbo.Trucks");
            DropForeignKey("dbo.Rentals", "ApplicationUserId", "dbo.AspNetUsers");
            DropIndex("dbo.Rentals", new[] { "TruckId" });
            DropIndex("dbo.Rentals", new[] { "ApplicationUserId" });
            DropColumn("dbo.Rentals", "TruckId");
            DropColumn("dbo.Rentals", "ApplicationUserId");
        }
    }
}
