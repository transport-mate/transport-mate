namespace Transportation_Company.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangedMaxMinTempToNullableInOrderModel : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Orders", "MinTemperature", c => c.Single());
            AlterColumn("dbo.Orders", "MaxTemperature", c => c.Single());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Orders", "MaxTemperature", c => c.Single(nullable: false));
            AlterColumn("dbo.Orders", "MinTemperature", c => c.Single(nullable: false));
        }
    }
}
