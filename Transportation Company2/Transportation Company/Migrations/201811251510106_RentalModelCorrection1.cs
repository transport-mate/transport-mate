namespace Transportation_Company.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RentalModelCorrection1 : DbMigration
    {
        public override void Up()
        {
          Sql(@"ALTER TABLE [dbo].[Rentals] DROP CONSTRAINT [FK_dbo.Rentals_dbo.Drivers_Driver_Id]");
            DropColumn("dbo.Rentals", "DriverId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Rentals", "DriverId", c => c.Int(nullable: false));

        }
    }
}
