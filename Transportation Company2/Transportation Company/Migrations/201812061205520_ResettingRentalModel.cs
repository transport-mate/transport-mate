namespace Transportation_Company.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ResettingRentalModel : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Rentals", "ApplicationUser_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.Rentals", "Truck_Id", "dbo.Trucks");
            DropIndex("dbo.Rentals", new[] { "ApplicationUser_Id" });
            DropIndex("dbo.Rentals", new[] { "Truck_Id" });
            DropColumn("dbo.Rentals", "ApplicationUser_Id");
            DropColumn("dbo.Rentals", "Truck_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Rentals", "Truck_Id", c => c.Int(nullable: false));
            AddColumn("dbo.Rentals", "ApplicationUser_Id", c => c.String(nullable: false, maxLength: 128));
            CreateIndex("dbo.Rentals", "Truck_Id");
            CreateIndex("dbo.Rentals", "ApplicationUser_Id");
            AddForeignKey("dbo.Rentals", "Truck_Id", "dbo.Trucks", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Rentals", "ApplicationUser_Id", "dbo.AspNetUsers", "Id", cascadeDelete: true);
        }
    }
}
