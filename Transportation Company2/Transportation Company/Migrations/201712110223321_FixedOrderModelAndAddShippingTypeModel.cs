namespace Transportation_Company.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FixedOrderModelAndAddShippingTypeModel : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ShippingTypes",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(),
                        Rate = c.Int(nullable: false),
                        Duration = c.Byte(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Orders", "ShippingType_Id", c => c.Long());
            CreateIndex("dbo.Orders", "ShippingType_Id");
            AddForeignKey("dbo.Orders", "ShippingType_Id", "dbo.ShippingTypes", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Orders", "ShippingType_Id", "dbo.ShippingTypes");
            DropIndex("dbo.Orders", new[] { "ShippingType_Id" });
            DropColumn("dbo.Orders", "ShippingType_Id");
            DropTable("dbo.ShippingTypes");
        }
    }
}
