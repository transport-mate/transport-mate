namespace Transportation_Company.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedPropertyToTrailersMaintenance : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Trailers", "NeedsMaintenance", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Trailers", "NeedsMaintenance");
        }
    }
}
