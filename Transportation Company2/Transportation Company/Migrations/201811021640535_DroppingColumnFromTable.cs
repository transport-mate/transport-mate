namespace Transportation_Company.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DroppingColumnFromTable : DbMigration
    {
        public override void Up()
        {
            Sql("DROP INDEX IF EXISTS IX_OrderStatus_Id ON Orders");
            Sql("ALTER TABLE Orders DROP CONSTRAINT IF EXISTS [FK_dbo.Orders_dbo.OrderStatus_OrderStatus_Id]");
            Sql("ALTER TABLE Orders DROP COLUMN OrderStatus_Id");
        }
        
        public override void Down()
        {
           
        }
    }
}
