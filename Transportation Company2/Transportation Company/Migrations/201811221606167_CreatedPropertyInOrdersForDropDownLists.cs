namespace Transportation_Company.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreatedPropertyInOrdersForDropDownLists : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "FullNaming", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Orders", "FullNaming");
        }
    }
}
