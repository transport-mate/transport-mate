namespace Transportation_Company.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OrderCorrectionAgain3 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "OrderStatusId", c => c.Byte(nullable: false));
            CreateIndex("dbo.Orders", "OrderStatusId");
            AddForeignKey("dbo.Orders", "OrderStatusId", "dbo.OrderStatus", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Orders", "OrderStatusId", "dbo.OrderStatus");
            DropIndex("dbo.Orders", new[] { "OrderStatusId" });
            DropColumn("dbo.Orders", "OrderStatusId");
        }
    }
}
