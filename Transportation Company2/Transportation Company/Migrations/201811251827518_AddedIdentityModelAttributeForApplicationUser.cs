namespace Transportation_Company.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedIdentityModelAttributeForApplicationUser : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "FullNameWithBirthDate", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "FullNameWithBirthDate");
        }
    }
}
