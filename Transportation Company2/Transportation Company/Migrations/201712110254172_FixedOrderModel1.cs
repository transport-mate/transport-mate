namespace Transportation_Company.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FixedOrderModel1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "CreatedOnDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Orders", "TrackingNumber", c => c.Guid(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Orders", "TrackingNumber");
            DropColumn("dbo.Orders", "CreatedOnDate");
        }
    }
}
