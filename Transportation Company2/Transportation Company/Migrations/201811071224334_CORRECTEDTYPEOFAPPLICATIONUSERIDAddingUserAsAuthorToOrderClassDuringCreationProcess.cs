namespace Transportation_Company.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CORRECTEDTYPEOFAPPLICATIONUSERIDAddingUserAsAuthorToOrderClassDuringCreationProcess : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "ApplicationUserId", c => c.String(maxLength: 128));
            CreateIndex("dbo.Orders", "ApplicationUserId");
            AddForeignKey("dbo.Orders", "ApplicationUserId", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Orders", "ApplicationUserId", "dbo.AspNetUsers");
            DropIndex("dbo.Orders", new[] { "ApplicationUserId" });
            DropColumn("dbo.Orders", "ApplicationUserId");
        }
    }
}
