namespace Transportation_Company.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCompanyNameToCustomerModel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Customers", "CompanyName", c => c.String(maxLength: 255));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Customers", "CompanyName");
        }
    }
}
