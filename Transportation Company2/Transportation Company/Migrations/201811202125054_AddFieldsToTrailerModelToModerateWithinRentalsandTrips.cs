namespace Transportation_Company.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFieldsToTrailerModelToModerateWithinRentalsandTrips : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Trailers", "IsAvailable", c => c.Boolean(nullable: false));
            AddColumn("dbo.Trailers", "NumberInStock", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Trailers", "NumberInStock");
            DropColumn("dbo.Trailers", "IsAvailable");
        }
    }
}
