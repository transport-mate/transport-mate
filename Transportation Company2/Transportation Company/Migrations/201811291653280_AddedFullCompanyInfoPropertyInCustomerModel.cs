namespace Transportation_Company.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedFullCompanyInfoPropertyInCustomerModel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Customers", "FullCustomerInfo", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Customers", "FullCustomerInfo");
        }
    }
}
