namespace Transportation_Company.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TripModelUpdated2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Trips", "Order_Id", "dbo.Orders");
            DropIndex("dbo.Trips", new[] { "Order_Id" });
            DropColumn("dbo.Trips", "OrderId");
            DropColumn("dbo.Trips", "Order_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Trips", "Order_Id", c => c.Long());
            AddColumn("dbo.Trips", "OrderId", c => c.Int(nullable: false));
            CreateIndex("dbo.Trips", "Order_Id");
            AddForeignKey("dbo.Trips", "Order_Id", "dbo.Orders", "Id");
        }
    }
}
