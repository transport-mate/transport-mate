namespace Transportation_Company.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateModelMakeClass : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ModelMakes", "MakeId", c => c.Int(nullable: false));
            CreateIndex("dbo.ModelMakes", "MakeId");
            AddForeignKey("dbo.ModelMakes", "MakeId", "dbo.Makes", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ModelMakes", "MakeId", "dbo.Makes");
            DropIndex("dbo.ModelMakes", new[] { "MakeId" });
            DropColumn("dbo.ModelMakes", "MakeId");
        }
    }
}
