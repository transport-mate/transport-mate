namespace Transportation_Company.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RentalsModel : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Rentals",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DateRented = c.DateTime(nullable: false),
                        DateReturned = c.DateTime(),
                        Driver_Id = c.Int(nullable: false),
                        Truck_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Drivers", t => t.Driver_Id, cascadeDelete: true)
                .ForeignKey("dbo.Trucks", t => t.Truck_Id, cascadeDelete: true)
                .Index(t => t.Driver_Id)
                .Index(t => t.Truck_Id);
            
            CreateTable(
                "dbo.Drivers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LicenseNumber = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Trucks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Engine = c.Single(nullable: false),
                        LiftingCapacity = c.Int(nullable: false),
                        LicencePlate = c.String(nullable: false),
                        Brand = c.String(nullable: false),
                        Model = c.String(nullable: false),
                        YearOfProduction = c.Int(nullable: false),
                        Driver_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Drivers", t => t.Driver_Id)
                .Index(t => t.Driver_Id);
            
            CreateTable(
                "dbo.Trailers",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Length = c.Int(nullable: false),
                        Capacity = c.Int(nullable: false),
                        LicencePlate = c.String(nullable: false),
                        Brand = c.String(nullable: false),
                        Model = c.String(nullable: false),
                        YearOfProduction = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Trucks", t => t.Id)
                .Index(t => t.Id);
            
            AddColumn("dbo.Orders", "IsRefrigerated", c => c.Boolean(nullable: false));
            AddColumn("dbo.Orders", "MinTemperature", c => c.Single(nullable: false));
            AddColumn("dbo.Orders", "MaxTemperature", c => c.Single(nullable: false));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Rentals", "Truck_Id", "dbo.Trucks");
            DropForeignKey("dbo.Trailers", "Id", "dbo.Trucks");
            DropForeignKey("dbo.Trucks", "Driver_Id", "dbo.Drivers");
            DropForeignKey("dbo.Rentals", "Driver_Id", "dbo.Drivers");
            DropIndex("dbo.Trailers", new[] { "Id" });
            DropIndex("dbo.Trucks", new[] { "Driver_Id" });
            DropIndex("dbo.Rentals", new[] { "Truck_Id" });
            DropIndex("dbo.Rentals", new[] { "Driver_Id" });
            DropColumn("dbo.Orders", "MaxTemperature");
            DropColumn("dbo.Orders", "MinTemperature");
            DropColumn("dbo.Orders", "IsRefrigerated");
            DropTable("dbo.Trailers");
            DropTable("dbo.Trucks");
            DropTable("dbo.Drivers");
            DropTable("dbo.Rentals");
        }
    }
}
