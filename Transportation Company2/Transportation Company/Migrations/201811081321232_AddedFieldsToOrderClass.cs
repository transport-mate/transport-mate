namespace Transportation_Company.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedFieldsToOrderClass : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "PickUpNumber", c => c.String());
            AddColumn("dbo.Orders", "DeliveryNumber", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Orders", "DeliveryNumber");
            DropColumn("dbo.Orders", "PickUpNumber");
        }
    }
}
