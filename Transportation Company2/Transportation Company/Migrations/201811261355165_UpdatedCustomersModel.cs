namespace Transportation_Company.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdatedCustomersModel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Customers", "Email", c => c.String(nullable: false));
            AddColumn("dbo.Customers", "PhoneNumber", c => c.String(nullable: false));
            AddColumn("dbo.Customers", "PhoneNumberExtension", c => c.String());
            AddColumn("dbo.Customers", "Comment", c => c.String(maxLength: 1200));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Customers", "Comment");
            DropColumn("dbo.Customers", "PhoneNumberExtension");
            DropColumn("dbo.Customers", "PhoneNumber");
            DropColumn("dbo.Customers", "Email");
        }
    }
}
