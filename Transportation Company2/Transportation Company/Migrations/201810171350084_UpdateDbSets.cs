namespace Transportation_Company.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateDbSets : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Rentals", name: "Driver_Id", newName: "DriverId");
            RenameIndex(table: "dbo.Rentals", name: "IX_Driver_Id", newName: "IX_DriverId");
            CreateTable(
                "dbo.Trailers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Length = c.Int(nullable: false),
                        Capacity = c.Int(nullable: false),
                        TrailerTypeId = c.Int(nullable: false),
                        LicencePlate = c.String(nullable: false),
                        Brand = c.String(nullable: false),
                        Model = c.String(nullable: false),
                        YearOfProduction = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.TrailerTypes", t => t.TrailerTypeId, cascadeDelete: true)
                .Index(t => t.TrailerTypeId);
            
            CreateTable(
                "dbo.Trips",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DriverId = c.Int(nullable: false),
                        OrderId = c.Int(nullable: false),
                        Order_Id = c.Long(nullable: false),
                        Trailer_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Drivers", t => t.DriverId, cascadeDelete: true)
                .ForeignKey("dbo.Orders", t => t.Order_Id, cascadeDelete: true)
                .ForeignKey("dbo.Trailers", t => t.Trailer_Id, cascadeDelete: true)
                .Index(t => t.DriverId)
                .Index(t => t.Order_Id)
                .Index(t => t.Trailer_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Trips", "Trailer_Id", "dbo.Trailers");
            DropForeignKey("dbo.Trips", "Order_Id", "dbo.Orders");
            DropForeignKey("dbo.Trips", "DriverId", "dbo.Drivers");
            DropForeignKey("dbo.Trailers", "TrailerTypeId", "dbo.TrailerTypes");
            DropIndex("dbo.Trips", new[] { "Trailer_Id" });
            DropIndex("dbo.Trips", new[] { "Order_Id" });
            DropIndex("dbo.Trips", new[] { "DriverId" });
            DropIndex("dbo.Trailers", new[] { "TrailerTypeId" });
            DropTable("dbo.Trips");
            DropTable("dbo.Trailers");
            RenameIndex(table: "dbo.Rentals", name: "IX_DriverId", newName: "IX_Driver_Id");
            RenameColumn(table: "dbo.Rentals", name: "DriverId", newName: "Driver_Id");
        }
    }
}
