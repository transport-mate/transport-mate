namespace Transportation_Company.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedFieldsToTruckToMaintainAvailability : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Trucks", "NumberAvailable", c => c.Byte(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Trucks", "NumberAvailable");
        }
    }
}
