namespace Transportation_Company.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CheckingIfTheAnnotationToShippingTypeIsOkay : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ShippingTypes", "Name", c => c.String(maxLength: 100));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ShippingTypes", "Name", c => c.String(nullable: false, maxLength: 100));
        }
    }
}
