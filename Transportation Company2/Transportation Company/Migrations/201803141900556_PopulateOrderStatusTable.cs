namespace Transportation_Company.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulateOrderStatusTable : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO OrderStatus (Name, Description) VALUES ('Accepted','Order has been accepted to the system')");
            Sql("INSERT INTO OrderStatus (Name, Description) VALUES ('Assigned','Order has been reviewed and assigned to the Driver')");
            Sql("INSERT INTO OrderStatus (Name, Description) VALUES ('Cancelled','Order has been cancelled before assigning to the Driver')");
            Sql("INSERT INTO OrderStatus (Name, Description) VALUES ('Picked-up','ORder has been picked-up by the Driver')");
            Sql("INSERT INTO OrderStatus (Name, Description) VALUES ('Delayed','Order has been marked by the Driver as the delayed')");
            Sql("INSERT INTO OrderStatus (Name, Description) VALUES ('Delivered','Order has been delivered by the Driver to the destination')");
            Sql("INSERT INTO OrderStatus (Name, Description) VALUES ('Broken','Order has been marked as broken and will be returned after')");
            Sql("INSERT INTO OrderStatus (Name, Description) VALUES ('Returned','Order was destroyed during the trip and has been transported back to the origin')");
            Sql("INSERT INTO OrderStatus (Name, Description) VALUES ('Completed','Order has been completed with no errors')");
        }
        
        public override void Down()
        {
        }
    }
}
