namespace Transportation_Company.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedIsTakenPropertyToTruckAndTrailerModels : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Trucks", "IsTaken", c => c.Boolean(nullable: false));
            AddColumn("dbo.Trailers", "IsTaken", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Trailers", "IsTaken");
            DropColumn("dbo.Trucks", "IsTaken");
        }
    }
}
