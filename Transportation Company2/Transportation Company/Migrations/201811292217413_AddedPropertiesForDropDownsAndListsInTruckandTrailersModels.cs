namespace Transportation_Company.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedPropertiesForDropDownsAndListsInTruckandTrailersModels : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Trucks", "TruckFullName", c => c.String());
            AddColumn("dbo.Trailers", "TrailerFullName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Trailers", "TrailerFullName");
            DropColumn("dbo.Trucks", "TruckFullName");
        }
    }
}
