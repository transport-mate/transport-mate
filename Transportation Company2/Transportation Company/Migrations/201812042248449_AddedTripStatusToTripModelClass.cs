namespace Transportation_Company.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedTripStatusToTripModelClass : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Trips", "TripStatusId", c => c.Byte(nullable: false));
            CreateIndex("dbo.Trips", "TripStatusId");
            AddForeignKey("dbo.Trips", "TripStatusId", "dbo.TripStatus", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Trips", "TripStatusId", "dbo.TripStatus");
            DropIndex("dbo.Trips", new[] { "TripStatusId" });
            DropColumn("dbo.Trips", "TripStatusId");
        }
    }
}
