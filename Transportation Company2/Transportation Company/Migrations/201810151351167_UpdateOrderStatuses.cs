namespace Transportation_Company.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateOrderStatuses : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "ProfilePicture", c => c.Binary());

            Sql("DELETE FROM OrderStatus where ID = 7");
            Sql("DELETE FROM OrderStatus where ID = 8");
            Sql("DELETE FROM OrderStatus where ID = 9");
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "ProfilePicture");
        }
    }
}
