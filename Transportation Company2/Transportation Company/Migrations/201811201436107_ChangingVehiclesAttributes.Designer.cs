// <auto-generated />
namespace Transportation_Company.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class ChangingVehiclesAttributes : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(ChangingVehiclesAttributes));
        
        string IMigrationMetadata.Id
        {
            get { return "201811201436107_ChangingVehiclesAttributes"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
