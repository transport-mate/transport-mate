namespace Transportation_Company.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddUwagiToOrdersModel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "AdditionalInformation", c => c.String(maxLength: 1500));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Orders", "AdditionalInformation");
        }
    }
}
