namespace Transportation_Company.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulateMembershipTypes : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO MembershipTypes (Id, Name, DurationInMonths, DiscountRate) VALUES (1, 'One time order', 0, 0)");
            Sql("INSERT INTO MembershipTypes (Id, Name, DurationInMonths, DiscountRate) VALUES (2, '1 month contract', 1, 5)");
            Sql("INSERT INTO MembershipTypes (Id, Name, DurationInMonths, DiscountRate) VALUES (3, '3 months contract', 3, 10)");
            Sql("INSERT INTO MembershipTypes (Id, Name, DurationInMonths, DiscountRate) VALUES (4, '6 months contract', 6, 15)");
            Sql("INSERT INTO MembershipTypes (Id, Name, DurationInMonths, DiscountRate) VALUES (5, '12 months contract', 12, 20)");
            Sql("INSERT INTO MembershipTypes (Id, Name, DurationInMonths, DiscountRate) VALUES (6, '24 months contract', 24, 40)");
        }
        
        public override void Down()
        {
        }
    }
}
