namespace Transportation_Company.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreatedClassForTruckMileageHistory2 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TruckMileageHistories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DateEntered = c.DateTime(nullable: false),
                        Mileage = c.Int(nullable: false),
                        TruckId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Trucks", t => t.TruckId, cascadeDelete: true)
                .Index(t => t.TruckId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TruckMileageHistories", "TruckId", "dbo.Trucks");
            DropIndex("dbo.TruckMileageHistories", new[] { "TruckId" });
            DropTable("dbo.TruckMileageHistories");
        }
    }
}
